module gitlab.com/woh-group/woh-backend/api-profesionales

require (
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.2.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.0.0
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.6 // indirect
	github.com/mongodb/mongo-go-driver v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/woh-group/woh-backend/db-mongo v0.0.0-20190226050748-f7bba143315d
	gitlab.com/woh-group/woh-backend/util-api v0.0.0-20190226050748-f7bba143315d
	gitlab.com/woh-group/woh-backend/util-auditoria v0.0.0-20190226050748-f7bba143315d
	gitlab.com/woh-group/woh-backend/util-auth v0.0.0-20190226050748-f7bba143315d
	gitlab.com/woh-group/woh-backend/util-logger v0.0.0-20190226050748-f7bba143315d
	go.mongodb.org/mongo-driver v1.0.0
	golang.org/x/crypto v0.0.0-20190228161510-8dd112bcdc25 // indirect
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6 // indirect
	golang.org/x/sys v0.0.0-20190228124157-a34e9553db1e // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/validator.v9 v9.27.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.2.2
)
