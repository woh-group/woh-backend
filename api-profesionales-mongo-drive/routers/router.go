package routers

import (
	echo "github.com/labstack/echo/v4"

	especialidadService "gitlab.com/woh-group/woh-backend/api-profesionales/services/especialidades"
	// profesionalService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional"
)

// PATH de la api
const PATH = "/api/profesionales"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {

	// create groups
	// profesionalGroup := e.Group(PATH + "/profesionales")
	especialidadGroup := e.Group(PATH + "/especialidades")

	// profesionalService.SetRouters(profesionalGroup)
	especialidadService.SetRouters(especialidadGroup)
}
