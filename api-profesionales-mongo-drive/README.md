# Descripción

Proyecto api de profesionales

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-profesionales/...

## Crear Modulo

export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-profesionales

## Docker

docker build -t api-profesionales .

docker build --no-cache -t api-profesionales .

docker run -it api-profesionales bash

docker run -it --name api-profesionales -p 3003:3003 api-profesionales

winpty docker run -it --name api-profesionales -p 3003:3003 api-profesionales