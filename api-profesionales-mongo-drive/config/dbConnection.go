package config

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

var database *mongo.Database

// Connection entidad
type Connection struct {
	Database      string `json:"database"`
	ConnectionURI string `json:"connectionURI"`
}

// DBCollection referencia la collection a trabajar
func getDBConnection(connection *Connection) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// client, err := mongo.Connect(context.TODO(), "mongodb://localhost:27017")
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connection.ConnectionURI))
	// defer client.Disconnect(ctx)
	if err != nil {
		return err
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return err
	}

	fmt.Println("Connected to MongoDB!", connection.Database, connection.ConnectionURI)

	// return client.Database("woh"), nil
	database = client.Database(connection.Database)
	return nil
}

func GetDatabase(connection *Connection) (*mongo.Database, error) {

	// connection := ConnectionDB{
	// 	Database:      "woh",
	// 	ConnectionURI: "mongodb://wohdb:wohDev++@localhost:27017",
	// }
	// connection := Connection{
	// 	Database:      "test",
	// 	ConnectionURI: "mongodb://localhost:27017",
	// }

	if database == nil {
		fmt.Println("ENTRO IF")
		if err := getDBConnection(connection); err != nil {
			return nil, err
		}
		fmt.Println("database", database)
	} else {
		fmt.Println("ENTRO ELSE")
	}

	return database, nil
}

func main() {

	connection := Connection{
		Database: "woh",
		// ConnectionURI: "mongodb://wohAdmin:K8rdEngYjRXH1wbWd4Dv@localhost:27017/admin",
		ConnectionURI: "mongodb://wohdb:wohDev++@localhost:27017/woh",
	}

	GetDatabase(&connection)
	FindExample()
	// GetDatabase()
	// GetDatabase()
	// GetDatabase()
	// GetDatabase()
	// GetDatabase()
	// GetDatabase()
	// InsertExamples()
}

// Especialidad entidad
type Especialidad struct {
	ID                  primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	Descripcion         string             `bson:"descripcion" json:"descripcion" validate:"required"`
	Estado              bool               `bson:"estado" json:"estado"`
	FechaCreacion       time.Time          `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string             `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time          `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string             `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// FindExample contains examples of update operations.
func FindExample() {
	collection := database.Collection("especialidades")

	{
		// Pass these options to the Find method
		findOptions := options.Find()
		findOptions.SetLimit(1)

		// Here's an array in which you can store the decoded documents
		// var results []Trainer
		var results []Especialidad

		// Passing nil as the filter matches all documents in the collection
		// cursor, err := collection.Find(context.TODO(), bson.D{})
		// cursor, err := collection.Find(context.TODO(), bsonx.Doc{}, findOptions)
		cursor, err := collection.Find(context.TODO(), bsonx.Doc{}, nil)
		if err != nil {
			log.Fatal("errorrr: ", err)
		}

		// Finding multiple documents returns a cursor
		// Iterating through the cursor allows us to decode documents one at a time
		for cursor.Next(context.TODO()) {

			// create a value into which the single document can be decoded
			var elem Especialidad
			err := cursor.Decode(&elem)
			if err != nil {
				log.Fatal(err)
			}
			results = append(results, elem)
		}

		if err := cursor.Err(); err != nil {
			log.Fatal(err)
		}

		// Close the cursor once finished
		cursor.Close(context.TODO())

		fmt.Printf("Found multiple documents: %+v\n", results)
	}

}
