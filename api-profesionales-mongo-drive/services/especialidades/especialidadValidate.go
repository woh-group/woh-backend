package especialidad

import (
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// ValidateRequestCreate valida un request
func ValidateRequestCreate(obj Especialidad) error {

	if err := apiUtil.IsNotEmpty(obj.Descripcion, "descripcion"); err != nil {
		return err
	}

	return nil
}

// ValidateRequestUpdate valida un request
func ValidateRequestUpdate(id string, obj Especialidad) error {

	if err := apiUtil.IsNotEmpty(obj.Descripcion, "descripcion"); err != nil {
		return err
	}

	return nil
}
