package especialidad

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"

	config "gitlab.com/woh-group/woh-backend/api-profesionales/config"
)

// CollectionName nombre de la coleccion
const (
	CollectionName = "especialidades"
)

var (
	unique = true
)

// EspecialidadRepository objeto entidad
type EspecialidadRepository struct {
	database   *mongo.Database
	collection *mongo.Collection
	err        error
}

var index = mongo.IndexModel{
	Keys: bsonx.Doc{
		{Key: "descripcion", Value: bsonx.Int32(int32(1))},
	},
	Options: &options.IndexOptions{
		Unique: &unique,
	},
}

func (repository *EspecialidadRepository) initContext() {
	connection := config.Connection(config.GetConnectionConfig().Database)
	repository.database, repository.err = config.GetDatabase(&connection)

	if repository.err == nil {
		repository.collection = repository.database.Collection(CollectionName)

		// Creamos el index
		opts := options.CreateIndexes().SetMaxTime(10 * time.Second)
		repository.collection.Indexes().CreateOne(context.Background(), index, opts)
	}
}

// FindAll devuelve todos los registros
func (repository EspecialidadRepository) FindAll(params map[string]interface{}, sort string, pageNumber int, pageSize int) (*[]Especialidad, error) {
	repository.initContext()
	if repository.err != nil {
		fmt.Println("ERROR 1:", repository.err)
		return nil, repository.err
	}

	var objs []Especialidad
	var err error

	findOptions := options.Find()
	findOptions.SetSkip(int64(pageNumber))
	findOptions.SetLimit(int64(pageSize))

	cursor, err := repository.collection.Find(context.TODO(), bsonx.Doc{}, findOptions)
	if err != nil {
		fmt.Println("ERROR 2:", err)
		return nil, err
	}

	for cursor.Next(context.TODO()) {

		// create a value into which the single document can be decoded
		var elem Especialidad
		err := cursor.Decode(&elem)
		if err != nil {
			fmt.Println("ERROR 3:", err)
			return nil, err
		}
		objs = append(objs, elem)
	}

	if err := cursor.Err(); err != nil {
		fmt.Println("ERROR 4:", err)
		return nil, err
	}

	// Close the cursor once finished
	cursor.Close(context.TODO())

	// query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	// err = query.All(&objs)

	fmt.Println("ERROR 5:", err)
	return &objs, err
}

// // FindByID devuelve un registro filtrado por Id
// func (repository EspecialidadRepository) FindByID(id string) (*Especialidad, error) {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return nil, repository.err
// 	}
// 	defer repository.context.Close()

// 	if !bson.IsObjectIdHex(id) {
// 		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
// 	}

// 	var obj Especialidad
// 	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
// 	return &obj, err
// }

// // Count devuelve la cantidad de registros
// func (repository EspecialidadRepository) Count(params map[string]interface{}) (int, error) {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return 0, repository.err
// 	}
// 	defer repository.context.Close()

// 	count, err := repository.c.Find(params).Count()
// 	return count, err
// }

// // Create inserta un nuevo registro
// func (repository EspecialidadRepository) Create(obj *Especialidad, usuarioCreacion string) error {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return repository.err
// 	}
// 	defer repository.context.Close()

// 	obj.ID = bson.NewObjectId()
// 	obj.FechaCreacion = time.Now()
// 	obj.UsuarioCreacion = usuarioCreacion
// 	obj.FechaModificacion = time.Time{}
// 	err := repository.c.Insert(obj)
// 	return err
// }

// // Update actualiza un registro
// func (repository EspecialidadRepository) Update(id string, obj *Especialidad, usuarioModificacion string) error {
// 	repository.initContext()
// 	defer repository.context.Close()

// 	if !bson.IsObjectIdHex(id) {
// 		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
// 	}

// 	obj.FechaModificacion = time.Now()
// 	obj.UsuarioModificacion = usuarioModificacion
// 	obj.ID = bson.ObjectIdHex(id)

// 	// err := repository.c.UpdateId(obj.ID, &obj)
// 	err := repository.c.UpdateId(obj.ID,
// 		bson.M{"$set": bson.M{
// 			"descripcion":         obj.Descripcion,
// 			"estado":              obj.Estado,
// 			"fechaModificacion":   obj.FechaModificacion,
// 			"usuarioModificacion": obj.UsuarioModificacion,
// 		}})
// 	return err
// }

// // Delete elimina un registro
// func (repository EspecialidadRepository) Delete(id string) error {
// 	repository.initContext()
// 	defer repository.context.Close()

// 	if !bson.IsObjectIdHex(id) {
// 		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
// 	}

// 	return repository.c.RemoveId(bson.ObjectIdHex(id))
// }

// // CreateMany agrega un array de objetos
// func (repository EspecialidadRepository) CreateMany(objs []Especialidad) error {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return repository.err
// 	}
// 	defer repository.context.Close()

// 	// Clear DB
// 	// repository.c.RemoveAll(bson.M{})
// 	repository.c.DropCollection()

// 	for _, obj := range objs {
// 		err := repository.c.Insert(obj)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }

// // RemoveAll elimina los registros de una coleccion
// func (repository EspecialidadRepository) RemoveAll() error {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return repository.err
// 	}
// 	defer repository.context.Close()

// 	// Clear DB
// 	_, err := repository.c.RemoveAll(nil)
// 	return err
// }

// // DropCollection elimina la coleccion completa
// func (repository EspecialidadRepository) DropCollection() error {
// 	repository.initContext()
// 	if repository.err != nil {
// 		return repository.err
// 	}
// 	defer repository.context.Close()

// 	// Delete Coleccion
// 	err := repository.c.DropCollection()
// 	return err
// }
