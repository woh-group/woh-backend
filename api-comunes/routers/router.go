package routers

import (
	echo "github.com/labstack/echo/v4"

	ciudadService "gitlab.com/woh-group/woh-backend/api-comunes/services/ciudad"
	correoService "gitlab.com/woh-group/woh-backend/api-comunes/services/correo"
	departamentoService "gitlab.com/woh-group/woh-backend/api-comunes/services/departamento"
	nivelAcademicoService "gitlab.com/woh-group/woh-backend/api-comunes/services/nivelAcademico"
	paisService "gitlab.com/woh-group/woh-backend/api-comunes/services/pais"
	profesionService "gitlab.com/woh-group/woh-backend/api-comunes/services/profesion"
	tipoDocumentoService "gitlab.com/woh-group/woh-backend/api-comunes/services/tipoDocumento"
)

// PATH de la api
const PATH = "/api/comunes"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {

	// create groups
	paisGroup := e.Group(PATH + "/paises")
	departamentoGroup := e.Group(PATH + "/departamentos")
	ciudadGroup := e.Group(PATH + "/ciudades")
	tipoDocumentoGroup := e.Group(PATH + "/tiposDocumento")
	nivelAcademicoGroup := e.Group(PATH + "/nivelesAcademicos")
	profesionGroup := e.Group(PATH + "/profesiones")
	correoGroup := e.Group(PATH + "/correos")

	paisService.SetRouters(paisGroup)
	departamentoService.SetRouters(departamentoGroup)
	ciudadService.SetRouters(ciudadGroup)
	tipoDocumentoService.SetRouters(tipoDocumentoGroup)
	nivelAcademicoService.SetRouters(nivelAcademicoGroup)
	profesionService.SetRouters(profesionGroup)
	correoService.SetRouters(correoGroup)
}
