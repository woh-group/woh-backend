package correo

import (
	"bytes"
	"html/template"

	gomail "gopkg.in/gomail.v2"

	config "gitlab.com/woh-group/woh-backend/api-comunes/config"
)

// CorreoService objeto entidad
type CorreoService struct{}

// Variables globales
const (
	remitenteCorreo = "noresponder@woh.com.co"
	remitenteNombre = "Words of hope"
)

// Send envía un nuevo correo
func (correoService CorreoService) Send(correo *Correo) error {
	smtp := config.GetConnectionConfig().SMTP
	path := config.GetPath()

	// Create a new message.
	message := gomail.NewMessage()

	if len(correo.NombrePlantilla) > 0 {
		templateHTML := template.Must(template.New(correo.NombrePlantilla + ".html").ParseGlob(path + "/plantillas/emails/*.html"))
		correo.Palabras["URL_IMAGENES"] = config.GetConstantConfig().URLImagenes

		var tpl bytes.Buffer
		if err := templateHTML.Execute(&tpl, correo.Palabras); err != nil {
			return err
		}
		result := tpl.String()

		// Set the main email part to use HTML.
		message.SetBody("text/html", result)
	} else {
		// Set the alternative part to plain text.
		message.SetBody("text/plain", correo.Mensaje)
	}

	// Validamos los remitentes
	if len(correo.RemitenteCorreo) == 0 {
		correo.RemitenteCorreo = remitenteCorreo
	}
	if len(correo.RemitenteNombre) == 0 {
		correo.RemitenteNombre = remitenteNombre
	}

	// Construct the message headers, including a Configuration Set and a Tag.
	message.SetHeaders(map[string][]string{
		"From":    {message.FormatAddress(correo.RemitenteCorreo, correo.RemitenteNombre)},
		"To":      {correo.Destinatario},
		"Subject": {correo.Asunto},
		// Comment or remove the next line if you are not using a configuration set
		//"X-SES-CONFIGURATION-SET": {ConfigSet},
		// Comment or remove the next line if you are not using custom tags
		// "X-SES-MESSAGE-TAGS": {Tags},
	})

	// Send the email.
	d := gomail.NewPlainDialer(smtp.Host, smtp.Port, smtp.User, smtp.Pass)

	// Display an error message if something goes wrong; otherwise,
	// display a message confirming that the message was sent.
	err := d.DialAndSend(message)
	return err
}
