package correo

import (
	"net/http"

	echo "github.com/labstack/echo/v4"

	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaUtil "gitlab.com/woh-group/woh-backend/util-auditoria/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
)

// CorreoHandler objeto
type CorreoHandler struct{}

// Variables globales
const (
	APIName = "correos"
)

// Send envía un nuevo correo
func (CorreoHandler) Send(c echo.Context) error {
	nombreMetodo := "Send"
	defer c.Request().Body.Close()
	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}
	
	// Transformamos el body a la entidad
	var obj Correo
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(APIName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}
	// Logueamos
	go logger.PrintRequest(APIName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(APIName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}
	// Llamamos al servicio
	var correoService = CorreoService{}
	if err := correoService.Send(&obj); err != nil {		
		// Logueamos
		go logger.PrintResponse(APIName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		// return c.NoContent(http.StatusInternalServerError)
		return c.JSONBlob(http.StatusInternalServerError, []byte(err.Error()))
	}
	// Auditamos
	go auditoriaUtil.Auditar(APIName, username, c.Request().Header, nil, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(APIName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusCreated, "")
	return c.NoContent(http.StatusCreated)
}
