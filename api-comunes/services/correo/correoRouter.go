package correo

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = CorreoHandler{}

	g.POST("/send", handler.Send)
}
