package tipoDocumento

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// TipoDocumento entidad
type TipoDocumento struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Id                  int           `bson:"id" json:"id" validate:"required,min=1"`
	Descripcion         string        `bson:"descripcion" json:"descripcion" validate:"required"`
	Abreviatura         string        `bson:"abreviatura" json:"abreviatura" validate:"required"`
	Estado              bool          `bson:"estado" json:"estado"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
