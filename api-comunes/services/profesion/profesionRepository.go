package profesion

import (
	"errors"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-comunes/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "profesiones"

// ProfesionRepository objeto entidad
type ProfesionRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

var indexUnique = mgo.Index{
	Key:    []string{"nombre"},
	Unique: true,
}

// Parametros
var typeFields = map[string]string{
	"id":     dbUtil.TypeString,
	"nombre": dbUtil.TypeString,
	"estado": dbUtil.TypeBoolean,
}

func (repository *ProfesionRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContextWithIndex(CollectionName, &conectionDB, indexUnique)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository ProfesionRepository) FindAll(c echo.Context) (*[]Profesion, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Profesion
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository ProfesionRepository) FindByID(id string) (*Profesion, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Profesion
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// Count devuelve la cantidad de registros
func (repository ProfesionRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository ProfesionRepository) Create(obj *Profesion, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository ProfesionRepository) Update(id string, obj *Profesion, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	// err := repository.c.UpdateId(obj.ID, &obj)
	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"id":                  obj.Id,
			"nombre":              obj.Nombre,
			"estado":              obj.Estado,
			"fechaModificacion":   obj.FechaModificacion,
			"usuarioModificacion": obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository ProfesionRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.RemoveId(bson.ObjectIdHex(id))
}

// CreateMany agrega un array de objetos
func (repository ProfesionRepository) CreateMany(objs []Profesion) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository ProfesionRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository ProfesionRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
