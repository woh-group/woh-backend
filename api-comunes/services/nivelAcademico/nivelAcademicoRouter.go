package nivelAcademico

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = NivelAcademicoHandler{}

	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/count", handler.Count)
	g.POST("", handler.Create)
	g.PUT("/:id", handler.Update)
	g.DELETE("/:id", handler.Delete)
}
