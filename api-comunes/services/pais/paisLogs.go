package pais

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// PaisLogs entidad
type PaisLogs struct {
	ID                  bson.ObjectId 	`bson:"_id,omitempty" json:"_id"`
	Fecha       		time.Time       `bson:"fecha" json:"fecha,omitempty"`
	Tipo              	string        	`bson:"tipo" json:"tipo,omitempty"`
	Level              	string        	`bson:"level" json:"level,omitempty"`
	RequestId          	string        	`bson:"requestId" json:"requestId,omitempty"`
	AppId          		string        	`bson:"appId" json:"appId,omitempty"`
	Username            string          `bson:"username" json:"username,omitempty"`
	IpCliente       	string     		`bson:"ipCliente" json:"ipCliente,omitempty"`
	IpServidor     		string        	`bson:"ipServidor" json:"ipServidor,omitempty"`
	Api  	 			string     		`bson:"api" json:"api,omitempty"`
	Metodo 				string        	`bson:"metodo" json:"metodo,omitempty"`
	Uri 				string        	`bson:"uri" json:"uri,omitempty"`
	Body 				string        	`bson:"body" json:"body,omitempty"`
}
