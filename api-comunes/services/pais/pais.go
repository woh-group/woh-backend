package pais

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Pais entidad
type Pais struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Id                  string        `bson:"id" json:"id" validate:"required"`
	Codigo              string        `bson:"codigo" json:"codigo" validate:"required"`
	Nombre              string        `bson:"nombre" json:"nombre" validate:"required"`
	Continente          string        `bson:"continente" json:"continente"`
	Indicativo          string        `bson:"indicativo" json:"indicativo"`
	Estado              bool          `bson:"estado" json:"estado"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
