package pais

import (
	echo "github.com/labstack/echo/v4"

	departamentoService "gitlab.com/woh-group/woh-backend/api-comunes/services/departamento"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = PaisHandler{}
	var handlerDepartamento = departamentoService.DepartamentoHandler{}

	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/count", handler.Count)
	g.GET("/:id/departamentos", handlerDepartamento.GetAllDepartamentosByPaisID)
	g.GET("/:id/departamentos/count", handlerDepartamento.CountDepartamentosByPaisID)
	g.POST("", handler.Create)
	g.PUT("/:id", handler.Update)
	g.DELETE("/:id", handler.Delete)
}
