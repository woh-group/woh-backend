package departamento

import (
	echo "github.com/labstack/echo/v4"

	ciudadService "gitlab.com/woh-group/woh-backend/api-comunes/services/ciudad"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = DepartamentoHandler{}
	var handlerCiudad = ciudadService.CiudadHandler{}

	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/count", handler.Count)
	g.GET("/:id/ciudades", handlerCiudad.GetAllCiudadesByDepartamentoID)
	g.GET("/:id/ciudades/count", handlerCiudad.CountCiudadesByDepartamentoID)
	g.POST("", handler.Create)
	g.PUT("/:id", handler.Update)
	g.DELETE("/:id", handler.Delete)
}
