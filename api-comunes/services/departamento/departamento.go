package departamento

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Departamento entidad
type Departamento struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Id                  string        `bson:"id" json:"id" validate:"required"`
	Nombre              string        `bson:"nombre" json:"nombre" validate:"required"`
	PaisID              string        `bson:"paisId" json:"paisId" validate:"required"`
	Estado              bool          `bson:"estado" json:"estado"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
