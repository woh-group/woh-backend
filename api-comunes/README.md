# Descripción

Proyecto api de comunes

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-comunes/...

## Crear Modulo

export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-comunes

## Docker

docker build -t api-comunes .

docker build --no-cache -t api-comunes .

docker run -it api-comunes bash

### Docker Linux

docker run -it --name api-comunes -p 3000:3000 api-comunes

### Docker Windows

winpty docker run -it --name api-comunes -p 3000:3000 api-comunes

### Docker con enviroment

#### En Linux

docker run -it --name api-comunes -p 3000:3000 -v ${WOH_CONFIG_PATH}:/config -e WOH_CONFIG_PATH=/config api-comunes
docker run -it --name api-comunes -p 3000:3000 -v /home/ubuntu/woh/_CONFIG_FILES:/config -e WOH_CONFIG_PATH=/config api-comunes

#### En Windows

docker run -it --name api-comunes -p 3000:3000 -v "%WOH_CONFIG_PATH%":/config -e WOH_CONFIG_PATH=/config api-comunes