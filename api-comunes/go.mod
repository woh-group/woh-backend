module gitlab.com/woh-group/woh-backend/api-comunes

require (
	github.com/labstack/echo/v4 v4.1.5
	gitlab.com/woh-group/woh-backend/api-preguntas v0.0.0-20190519062713-f8cce431290b // indirect
	gitlab.com/woh-group/woh-backend/api-profesionales v0.0.0-20190519062713-f8cce431290b // indirect
	gitlab.com/woh-group/woh-backend/api-usuarios v0.0.0-20190519062713-f8cce431290b // indirect
	gitlab.com/woh-group/woh-backend/db-mongo v0.0.0-20190604010528-17c6582ed597
	gitlab.com/woh-group/woh-backend/util v0.0.0-20190519062713-f8cce431290b // indirect
	gitlab.com/woh-group/woh-backend/util-api v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-auditoria v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-auth v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-logger v0.0.0-20190621181500-9ad9ca5b56b1
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.2.2
)
