package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-comunes/config"
	ciudadService "gitlab.com/woh-group/woh-backend/api-comunes/services/ciudad"
	departamentoService "gitlab.com/woh-group/woh-backend/api-comunes/services/departamento"
	nivelAcademicoService "gitlab.com/woh-group/woh-backend/api-comunes/services/nivelAcademico"
	paisService "gitlab.com/woh-group/woh-backend/api-comunes/services/pais"
	profesionService "gitlab.com/woh-group/woh-backend/api-comunes/services/profesion"
	tipoDocumentoService "gitlab.com/woh-group/woh-backend/api-comunes/services/tipoDocumento"
)

func main() {

	// Cargamos la configuracion inicial
	config.LoadConfigFile()

	var paises, errPais = getPaises()
	if errPais != nil {
		fmt.Println("ERROR ->", errPais)
	}
	// fmt.Println("PAISES ->", paises)
	var paisRepository = paisService.PaisRepository{}
	if errPais = paisRepository.CreateMany(paises); errPais != nil {
		fmt.Println("ERROR ->", errPais)
	} else {
		fmt.Println("PAISES cargados exitosamente!")
	}

	fmt.Println("---------------")

	var departamentos, errDepartamento = getDepartamentos()
	if errDepartamento != nil {
		fmt.Println("ERROR ->", errDepartamento)
	}
	// fmt.Println("DEPARTAMENTOS ->", departamentos)
	var departamentoRepository = departamentoService.DepartamentoRepository{}
	if errDepartamento = departamentoRepository.CreateMany(departamentos); errDepartamento != nil {
		fmt.Println("ERROR ->", errDepartamento)
	} else {
		fmt.Println("DEPARTAMENTOS cargados exitosamente!")
	}

	fmt.Println("---------------")

	var ciudades, errCiudad = getCiudades()
	if errCiudad != nil {
		fmt.Println("ERROR ->", errCiudad)
	}
	// fmt.Println("CIUDADES ->", ciudades)
	var ciudadRepository = ciudadService.CiudadRepository{}
	if errCiudad = ciudadRepository.CreateMany(ciudades); errCiudad != nil {
		fmt.Println("ERROR ->", errCiudad)
	} else {
		fmt.Println("CIUDADES cargadas exitosamente!")
	}

	fmt.Println("---------------")

	var tiposDocumento, errTipoDocumento = getTiposDocumento()
	if errTipoDocumento != nil {
		fmt.Println("ERROR ->", errTipoDocumento)
	}
	// fmt.Println("TIPOS DE DOCUMENTOS ->", tiposDocumento)
	var tipoDocumentoRepository = tipoDocumentoService.TipoDocumentoRepository{}
	if errTipoDocumento = tipoDocumentoRepository.CreateMany(tiposDocumento); errTipoDocumento != nil {
		fmt.Println("ERROR ->", errTipoDocumento)
	} else {
		fmt.Println("TIPOS DE DOCUMENTO cargados exitosamente!")
	}

	fmt.Println("---------------")

	var nivelesAcademico, errNivelAcademico = getNivelesAcademicos()
	if errNivelAcademico != nil {
		fmt.Println("ERROR ->", errNivelAcademico)
	}
	// fmt.Println("TIPOS DE DOCUMENTOS ->", tiposDocumento)
	var nivelAcademicoRepository = nivelAcademicoService.NivelAcademicoRepository{}
	if errNivelAcademico = nivelAcademicoRepository.CreateMany(nivelesAcademico); errNivelAcademico != nil {
		fmt.Println("ERROR ->", errNivelAcademico)
	} else {
		fmt.Println("NIVELES ACADEMICOS cargados exitosamente!")
	}

	fmt.Println("---------------")

	var profesiones, errProfesion = getProfesiones()
	if errProfesion != nil {
		fmt.Println("ERROR ->", errProfesion)
	}
	var profesionRepository = profesionService.ProfesionRepository{}
	if errProfesion = profesionRepository.CreateMany(profesiones); errProfesion != nil {
		fmt.Println("ERROR ->", errProfesion)
	} else {
		fmt.Println("PROFESIONES cargadas exitosamente!")
	}
}

func getPaises() ([]paisService.Pais, error) {
	objs, err := ioutil.ReadFile("jsons/paises.json")

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	var paises []paisService.Pais
	err = json.Unmarshal(objs, &paises)

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	for i := range paises {
		paises[i].ID = bson.NewObjectId()
		paises[i].FechaCreacion = time.Now()
		paises[i].UsuarioCreacion = "admin"
		paises[i].FechaModificacion = time.Time{}
	}

	return paises, nil
}

func getDepartamentos() ([]departamentoService.Departamento, error) {
	objs, err := ioutil.ReadFile("jsons/departamentos.json")

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	var departamentos []departamentoService.Departamento
	err = json.Unmarshal(objs, &departamentos)

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	for i := range departamentos {
		departamentos[i].ID = bson.NewObjectId()
		departamentos[i].FechaCreacion = time.Now()
		departamentos[i].UsuarioCreacion = "admin"
		departamentos[i].FechaModificacion = time.Time{}
		// fmt.Println("ID ->", departamentos[i].Id)
	}

	return departamentos, nil
}

func getCiudades() ([]ciudadService.Ciudad, error) {
	objs, err := ioutil.ReadFile("jsons/ciudades.json")

	if err != nil {
		return nil, err
	}

	var ciudades []ciudadService.Ciudad
	err = json.Unmarshal(objs, &ciudades)

	if err != nil {
		return nil, err
	}

	for i := range ciudades {
		ciudades[i].ID = bson.NewObjectId()
		ciudades[i].FechaCreacion = time.Now()
		ciudades[i].UsuarioCreacion = "admin"
		ciudades[i].FechaModificacion = time.Time{}
	}

	return ciudades, nil
}

func getTiposDocumento() ([]tipoDocumentoService.TipoDocumento, error) {
	objs, err := ioutil.ReadFile("jsons/tiposDocumento.json")

	if err != nil {
		return nil, err
	}

	var tiposDocumento []tipoDocumentoService.TipoDocumento
	err = json.Unmarshal(objs, &tiposDocumento)

	if err != nil {
		return nil, err
	}

	for i := range tiposDocumento {
		tiposDocumento[i].ID = bson.NewObjectId()
		tiposDocumento[i].FechaCreacion = time.Now()
		tiposDocumento[i].UsuarioCreacion = "admin"
		tiposDocumento[i].FechaModificacion = time.Time{}
	}

	return tiposDocumento, nil
}

func getNivelesAcademicos() ([]nivelAcademicoService.NivelAcademico, error) {
	objs, err := ioutil.ReadFile("jsons/nivelesAcademicos.json")

	if err != nil {
		return nil, err
	}

	var nivelesAcademico []nivelAcademicoService.NivelAcademico
	err = json.Unmarshal(objs, &nivelesAcademico)

	if err != nil {
		return nil, err
	}

	for i := range nivelesAcademico {
		nivelesAcademico[i].ID = bson.NewObjectId()
		nivelesAcademico[i].FechaCreacion = time.Now()
		nivelesAcademico[i].UsuarioCreacion = "admin"
		nivelesAcademico[i].FechaModificacion = time.Time{}
	}

	return nivelesAcademico, nil
}

func getProfesiones() ([]profesionService.Profesion, error) {
	objs, err := ioutil.ReadFile("jsons/profesiones.json")

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	var profesiones []profesionService.Profesion
	err = json.Unmarshal(objs, &profesiones)

	if err != nil {
		return nil, err
	}

	for i := range profesiones {
		profesiones[i].ID = bson.NewObjectId()
		profesiones[i].FechaCreacion = time.Now()
		profesiones[i].UsuarioCreacion = "admin"
		profesiones[i].FechaModificacion = time.Time{}
	}

	return profesiones, nil
}
