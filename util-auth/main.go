package main

import (
    "fmt"
    authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
)

func main() {

    permisos := map[string]bool{"READ": true, "ADD": true, "UPDATE": true, "DELETE": true}
    token := authUtil.CreateToken("COMUNES", authUtil.RolComunes, permisos, "ALL")
    fmt.Println("AccessToken:", token.AccessToken)
    fmt.Println("TokenType:", token.TokenType)
    fmt.Println("ExpiresIn:", token.ExpiresIn)
    fmt.Println("RefreshToken:", token.RefreshToken)
    fmt.Println("password:", authUtil.GetSecurityConfig().SecretKeyAccessToken)
}