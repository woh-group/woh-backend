package util

import (
	"fmt"
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

// Constantes
var (
	securityConfigFile = "securityConfig.yaml"
)

// SecurityConfig file
type SecurityConfig struct {
	SecretKeyAccessToken  string `yaml:"secretKeyAccessToken" json:"secretKeyAccessToken"`
	SecretKeyRefreshToken string `yaml:"secretKeyRefreshToken" json:"secretKeyRefreshToken"`
	TokenType             string `yaml:"tokenType" json:"tokenType"`
	TokenExpire           int    `yaml:"tokenExpire" json:"tokenExpire"`
}

var securityConfig SecurityConfig

// GetSecurityConfig obtiene la configuracion del servicio
func GetSecurityConfig() *SecurityConfig {
	// Validamos si la config esta vacia
	if (SecurityConfig{}) == securityConfig {
		readSecurityConfigFile()
	}
	return &securityConfig
}

// readSecurityConfigFile read security config file
func readSecurityConfigFile() {

	configPath := getPath()
	fileContent, err := ioutil.ReadFile(configPath + "/" + securityConfigFile)
	if err != nil {
		fmt.Printf("Error read config file: %v\n", err)
		panic(err)
	}

	// expand environment variables
	fileContent = []byte(os.ExpandEnv(string(fileContent)))
	if err := yaml.Unmarshal(fileContent, &securityConfig); err != nil {
		fmt.Printf("Error Unmarshal: %v\n", err)
		panic(err)
	}

	// fmt.Printf("Load security config: %v\n", securityConfig)
}

func getPath() string {
	// Obtenemos la ruta de la env sino devolvemos la ruta por defecto
	envConfigPath := os.Getenv("WOH_CONFIG_PATH")
	if len(envConfigPath) > 0 {
		return envConfigPath
	}
	return "config"
}
