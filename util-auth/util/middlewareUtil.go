package util

import (
	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// SetJwtMiddleware middleware que valida el token
func SetJwtMiddleware(e *echo.Echo) {
	e.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: Algorithm,
		AuthScheme:    GetSecurityConfig().TokenType,
		SigningKey:    []byte(GetSecurityConfig().SecretKeyAccessToken),
	}))
}
