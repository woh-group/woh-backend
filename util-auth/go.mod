module gitlab.com/woh-group/woh-backend/util-auth

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.0.0
	github.com/stretchr/objx v0.1.1 // indirect
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613 // indirect
	golang.org/x/sys v0.0.0-20190203050204-7ae0202eb74c // indirect
	gopkg.in/yaml.v2 v2.2.2
)
