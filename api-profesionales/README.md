# Descripción

Proyecto api de profesionales

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-profesionales/...

## Crear Modulo

export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-profesionales

## Docker

docker build -t api-profesionales .

docker build --no-cache -t api-profesionales .

docker run -it api-profesionales bash

### Docker Linux

docker run -it --name api-profesionales -p 3002:3002 api-profesionales

### Docker Windows

winpty docker run -it --name api-profesionales -p 3002:3002 api-profesionales

### Docker con enviroment

#### En Linux

docker run -it --name api-profesionales -p 3002:3002 -v ${WOH_CONFIG_PATH}:/config -e WOH_CONFIG_PATH=/config api-profesionales
docker run -it --name api-profesionales -p 3002:3002 -v /home/ubuntu/woh/_CONFIG_FILES:/config -e WOH_CONFIG_PATH=/config api-profesionales

#### En Windows

docker run -it --name api-profesionales -p 3002:3002 -v "%WOH_CONFIG_PATH%":/config -e WOH_CONFIG_PATH=/config api-profesionales