package routers

import (
	echo "github.com/labstack/echo/v4"

	especialidadService "gitlab.com/woh-group/woh-backend/api-profesionales/services/especialidades"
	profesionalService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional"
	profesionalEstudioService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-estudio"
	profesionalExperienciaService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-experiencia"
)

// PATH de la api
const PATH = "/api/profesionales"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {

	// create groups
	especialidadGroup := e.Group(PATH + "/especialidades")
	profesionalGroup := e.Group(PATH + "/profesionales")
	profesionalEstudioGroup := e.Group(PATH + "/profesionales/estudios")
	profesionalprofesionalExperienciaServiceGroup := e.Group(PATH + "/profesionales/experiencias")

	especialidadService.SetRouters(especialidadGroup)
	profesionalService.SetRouters(profesionalGroup)
	profesionalEstudioService.SetRouters(profesionalEstudioGroup)
	profesionalExperienciaService.SetRouters(profesionalprofesionalExperienciaServiceGroup)
}
