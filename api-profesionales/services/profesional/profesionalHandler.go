package profesional

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"

	echo "github.com/labstack/echo/v4"
	mgo "gopkg.in/mgo.v2"

	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaUtil "gitlab.com/woh-group/woh-backend/util-auditoria/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"

	estudioService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-estudio"
	experienciaService "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-experiencia"
)

// ProfesionalHandler objeto
type ProfesionalHandler struct{}

// Variables globales
const (
	ID            = "id"
	usuarioID     = "usuarioId"
	PerfilID      = "perfilId"
	EstudioID     = "estudioId"
	ExperienciaID = "experienciaId"
	IdiomaID      = "idiomaId"
	APIName       = "profesionales"
)

// GetAll obtiene todos los registros
func (ProfesionalHandler) GetAll(c echo.Context) error {
	nombreMetodo := "GetAll"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	var idUsuarios string
	if len(c.QueryParam("firstLetter")) > 0 {
		var usuarios []Usuario
		
		statusCode, err := getAllUsuariosByFirstLetter(c, &usuarios)
		if err == nil {
			for i, obj := range usuarios {
				if i > 0 {
					idUsuarios += "," + obj.ID.Hex()
				} else {
					idUsuarios += obj.ID.Hex()
				}
			}
		} else {
			go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Ocurrio al buscar los usuarios cuyo nombre empiece en:" + c.QueryParam("firstLetter"), nil, nil, statusCode, err)
		}
		if len(idUsuarios) > 0 {
			c.QueryParams()["usuarioId"]=[]string{idUsuarios}
		} else {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
			return c.NoContent(http.StatusOK)
		}
	}

	// Buscamos en la BD
	var profesionalRepository = ProfesionalRepository{}
	objs, err := profesionalRepository.FindAll(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Mapeamos los datos de la persona a los profesionales
	showPersona, _ := strconv.ParseBool(c.QueryParam("showPersona"))
	if showPersona {
		profesionales := *objs
		if len(profesionales) > 0 {
			var wg sync.WaitGroup
			wg.Add(len(profesionales))

			for i := range profesionales {
				go func(i int) {
					defer wg.Done()
					asignPersonaToProfesional(&profesionales[i], c)
				}(i)
			}
			wg.Wait()
		}
	}	

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, objs)
}

// GetByID obtiene un registro por Id
func (ProfesionalHandler) GetByID(c echo.Context) error {
	nombreMetodo := "GetById"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Buscamos en la BD
	var profesionalRepository = ProfesionalRepository{}
	obj, err := profesionalRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Mapeamos los datos de la persona al profesional
	showPersona, _ := strconv.ParseBool(c.QueryParam("showPersona"))
	if showPersona {
		asignPersonaToProfesional(obj, c)
	}

	// Mapeamos los estudios y estudios complementarios a los perfil
	showEstudios, _ := strconv.ParseBool(c.QueryParam("showEstudios"))
	if showEstudios {
		asignEstudiosToPerfil(obj.Perfiles, c)
	}

	// Mapeamos las experiencias a los perfil
	showExperiencias, _ := strconv.ParseBool(c.QueryParam("showExperiencias"))
	if showExperiencias {
		asignExperienciasToPerfil(obj.Perfiles, c)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// GetByUsuarioID obtiene un registro por usuarioId
func (ProfesionalHandler) GetByUsuarioID(c echo.Context) error {
	nombreMetodo := "GetByUsuarioID"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(usuarioID)

	// Buscamos en la BD
	var profesionalRepository = ProfesionalRepository{}
	obj, err := profesionalRepository.FindByUsuarioID(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Mapeamos los datos de la persona al profesional
	showPersona, _ := strconv.ParseBool(c.QueryParam("showPersona"))
	if showPersona {
		asignPersonaToProfesional(obj, c)
	}

	// Mapeamos los estudios y estudios complementarios a los perfil
	showEstudios, _ := strconv.ParseBool(c.QueryParam("showEstudios"))
	if showEstudios {
		asignEstudiosToPerfil(obj.Perfiles, c)
	}

	// Mapeamos las experiencias a los perfil
	showExperiencias, _ := strconv.ParseBool(c.QueryParam("showExperiencias"))
	if showExperiencias {
		asignExperienciasToPerfil(obj.Perfiles, c)
	}

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Count obtiene la cantidad de registros
func (ProfesionalHandler) Count(c echo.Context) error {
	nombreMetodo := "Count"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var profesionalRepository = ProfesionalRepository{}
	count, err := profesionalRepository.Count(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, count)
}

// Create inserta un nuevo registro
func (ProfesionalHandler) Create(c echo.Context) error {
	nombreMetodo := "Create"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj Profesional
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Agregamos a la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.Create(&obj, username); err != nil {
		if strings.Contains(err.Error(), dbConstantes.CodeDuplicateKey) {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusConflict, err.Error())
			return c.JSON(http.StatusConflict, dbConstantes.ErrorDatabaseDuplicateKey)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

// Update actualiza un registro
func (ProfesionalHandler) Update(c echo.Context) error {
	nombreMetodo := "Update"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Profesional
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.Update(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Delete elimina un registro
func (ProfesionalHandler) Delete(c echo.Context) error {
	nombreMetodo := "Delete"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Eliminamos de la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.Delete(id); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// ******** PERFIL PROFESIONAL ********

// UpdateIdiomas actualiza un registro
func (ProfesionalHandler) UpdateIdiomas(c echo.Context) error {
	nombreMetodo := "UpdateIdiomas"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Profesional
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.UpdateIdiomas(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// ******** PERFIL PROFESIONAL ********

// AddPerfilToProfesional agrega un nuevo perfil profesional
func (ProfesionalHandler) AddPerfilToProfesional(c echo.Context) error {
	nombreMetodo := "AddPerfilToProfesional"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj PerfilProfesional
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Obtenemos los parametros
	profesionalID := c.Param(ID)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Agregamos a la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.AddPerfilToProfesional(profesionalID, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		} else if strings.Contains(err.Error(), dbConstantes.CodeDuplicateKey) {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusConflict, err.Error())
			return c.JSON(http.StatusConflict, dbConstantes.ErrorDatabaseDuplicateKey)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

// UpdatePerfilToProfesional modifica un perfil profesional existente
func (ProfesionalHandler) UpdatePerfilToProfesional(c echo.Context) error {
	nombreMetodo := "UpdatePerfilToProfesional"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj PerfilProfesional
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Obtenemos los parametros
	profesionalID := c.Param(ID)
	perfilID := c.Param(PerfilID)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.UpdatePerfilToProfesional(profesionalID, perfilID, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// DeletePerfilToProfesional elimina un perfil profesional
func (ProfesionalHandler) DeletePerfilToProfesional(c echo.Context) error {
	nombreMetodo := "DeletePerfilToProfesional"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	profesionalID := c.Param(ID)
	perfilID := c.Param(PerfilID)

	// Eliminamos de la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.DeletePerfilToProfesional(profesionalID, perfilID); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, perfilID, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// ******** ESPECIALIDADES ********

// UpdateEspecialidadesToPerfil actualiza las especialidades de un perfil profesional
func (ProfesionalHandler) UpdateEspecialidadesToPerfil(c echo.Context) error {
	nombreMetodo := "UpdateEspecialidadesToPerfil"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos en requestBody en la entidad
	var obj []string
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Obtenemos los parametros
	profesionalID := c.Param(ID)
	perfilID := c.Param(PerfilID)

	// Actualizamos la BD
	var profesionalRepository = ProfesionalRepository{}
	if err := profesionalRepository.UpdateEspecialidadesToPerfil(profesionalID, perfilID, obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, perfilID, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// ******** OTROS ********

// asignPersonasToProfesionales asigna la persona que haya respondido a un array de respuestas
func asignPersonasToProfesionales(profesionales []Profesional, c echo.Context) {
	if len(profesionales) > 0 {
		var wg sync.WaitGroup
		wg.Add(len(profesionales))

		for i := range profesionales {
			go func(i int) {
				defer wg.Done()
				asignPersonaToProfesional(&profesionales[i], c)
			}(i)
		}
		wg.Wait()
	}
}

// asignPersonaToProfesional asigna la persona a un profesional
func asignPersonaToProfesional(profesional *Profesional, c echo.Context) {
	var persona Persona
	statusCode, err := getPersonaByUsuarioID(c, profesional.UsuarioID, &persona)
	if err != nil {
		go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, "asignPersonaToProfesional", "Ocurrio un error al buscar la persona asociada al profesional", nil, nil, statusCode, err)
		// fmt.Println("Error buscando la persona asociada a la respuesta:", err)
	}
	if statusCode == 200 {
		profesional.Persona = persona
	}
}

// asignEstudiosToPerfil asigna los estudios y estudios complementarios a un perfil
func asignEstudiosToPerfil(perfiles []PerfilProfesional, c echo.Context) {
	if len(perfiles) > 0 {
		var wg sync.WaitGroup
		wg.Add(len(perfiles))
		endpoint := apiUtil.GetEndpointConfig().EndpointAPIProfesionalesEstudios

		for i := range perfiles {
			go func(i int) {
				defer wg.Done()
				url := fmt.Sprintf("%s%s%s", endpoint, "/getByPerfilProfesionalId/", perfiles[i].ID.Hex())
				var estudios []estudioService.Estudio

				statusCode, err := apiUtil.GetResponse(apiUtil.Get, url, apiUtil.GetEndpointConfig().TimeoutAPIProfesionalesEstudios, c.Request().Header, nil, &estudios)
				if err != nil {
					go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, "asignEstudiosToPerfil", "Ocurrio un error al buscar los estudios asociados al perfil", nil, nil, statusCode, err)
					// fmt.Println("Error buscando los estudios asociados al perfil:", err)
				}
				if statusCode == 200 {
					for _, estudio := range estudios {
						if estudio.TipoEstudioID == 1 {
							// perfiles[i].Estudios = estudios
							perfiles[i].Estudios = append(perfiles[i].Estudios, estudio)
						} else if estudio.TipoEstudioID == 2 {
							// perfiles[i].EstudiosComplementarios = estudios
							perfiles[i].EstudiosComplementarios = append(perfiles[i].EstudiosComplementarios, estudio)
						}
					}
				}
			}(i)
		}
		wg.Wait()
	}
}

// asignExperienciasToPerfil asigna las experiencias a un perfil
func asignExperienciasToPerfil(perfiles []PerfilProfesional, c echo.Context) {
	if len(perfiles) > 0 {
		var wg sync.WaitGroup
		wg.Add(len(perfiles))
		endpoint := apiUtil.GetEndpointConfig().EndpointAPIProfesionalesExperiencias

		for i := range perfiles {
			go func(i int) {
				defer wg.Done()
				url := fmt.Sprintf("%s%s%s", endpoint, "/getByPerfilProfesionalId/", perfiles[i].ID.Hex())
				var experiencias []experienciaService.Experiencia

				statusCode, err := apiUtil.GetResponse(apiUtil.Get, url, apiUtil.GetEndpointConfig().TimeoutAPIProfesionalesExperiencias, c.Request().Header, nil, &experiencias)
				if err != nil {
					go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, "asignExperienciasToPerfil", "Ocurrio un error al buscar las experiencias asociadas al perfil", nil, nil, statusCode, err)
					// fmt.Println("Error buscando las experiencias asociados al perfil:", err)
				}
				if statusCode == 200 {
					perfiles[i].Experiencias = experiencias
				}
			}(i)
		}
		wg.Wait()
	}
}

// getAllUsuariosByFirstLetter obtiene una lista de usuarios por letra del nombre
func getAllUsuariosByFirstLetter(c echo.Context, usuarios *[]Usuario) (int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPIUsuarios

	// Obtenemos el parametros
	sort := "persona.nombres"
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)
	firstLetter := c.QueryParam("firstLetter")

	url := fmt.Sprintf("%s%s%s%s%v%s%v%s%s", endpoint, "?persona.nombres[startswith]=", firstLetter, "&rol=3&estadoId=4&page[number]=", pageNumber, "&page[size]=", pageSize, "&sort=", sort)
	return apiUtil.GetResponse(apiUtil.Get, url, apiUtil.GetEndpointConfig().TimeoutAPIUsuarios, c.Request().Header, nil, &usuarios)
}

// getPersonaByUsuarioID obtiene una persona
func getPersonaByUsuarioID(c echo.Context, usuarioID string, persona *Persona) (int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPIUsuarios
	url := fmt.Sprintf("%s%s%s", endpoint, "/getPersonaByUsuarioId/", usuarioID)

	return apiUtil.GetResponse(apiUtil.Get, url, apiUtil.GetEndpointConfig().TimeoutAPIUsuarios, c.Request().Header, nil, &persona)
}
