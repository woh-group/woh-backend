package profesional

import (
	"errors"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-profesionales/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "profesionales"

// ProfesionalRepository objeto entidad
type ProfesionalRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

var indexUnique = mgo.Index{
	Key:    []string{"usuarioId"},
	Unique: true,
}

// Parametros
var typeFields = map[string]string{
	"usuarioId": dbUtil.TypeString,
	"estado":    dbUtil.TypeBoolean,
}

func (repository *ProfesionalRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContextWithIndex(CollectionName, &conectionDB, indexUnique)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository ProfesionalRepository) FindAll(c echo.Context) (*[]Profesional, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Profesional
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	// Validamos si es una busqueda basada por letra
	if len(c.QueryParam("firstLetter")) > 0 {
		pageNumber = 1
	}

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository ProfesionalRepository) FindByID(id string) (*Profesional, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Profesional
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// FindByUsuarioID devuelve un registro filtrado por usuarioId
func (repository ProfesionalRepository) FindByUsuarioID(usuarioID string) (*Profesional, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var obj Profesional
	err := repository.c.Find(bson.M{"usuarioId": usuarioID}).One(&obj)
	return &obj, err
}

// Count devuelve la cantidad de registros
func (repository ProfesionalRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository ProfesionalRepository) Create(obj *Profesional, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	obj.ID = bson.NewObjectId()
	obj.Estado = true
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository ProfesionalRepository) Update(id string, obj *Profesional, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"especialidadPrincipal": obj.EspecialidadPrincipal,
			"fechaModificacion":     obj.FechaModificacion,
			"usuarioModificacion":   obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository ProfesionalRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.RemoveId(bson.ObjectIdHex(id))
}

// ******** IDIOMAS ********

// Update actualiza un registro
func (repository ProfesionalRepository) UpdateIdiomas(id string, obj *Profesional, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"idiomas":             obj.Idiomas,
			"fechaModificacion":   obj.FechaModificacion,
			"usuarioModificacion": obj.UsuarioModificacion,
		}})
	return err
}

// ******** PERFIL PROFESIONAL ********

// AddPerfilToProfesional agrega un nuevo perfil profesional
func (repository ProfesionalRepository) AddPerfilToProfesional(profesionalID string, obj *PerfilProfesional, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(profesionalID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}

	change := bson.M{"$push": bson.M{"perfiles": obj}}
	err := repository.c.UpdateId(bson.ObjectIdHex(profesionalID), change)

	return err
}

// UpdatePerfilToProfesional modifica un perfil profesional existente
func (repository ProfesionalRepository) UpdatePerfilToProfesional(profesionalID string, perfilID string, obj *PerfilProfesional, usuarioModificacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(profesionalID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(perfilID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.ID = bson.ObjectIdHex(perfilID)
	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion

	query := bson.M{"_id": bson.ObjectIdHex(profesionalID), "perfiles._id": bson.ObjectIdHex(perfilID)}
	change := bson.M{"$set": bson.M{
		"perfiles.$.profesionId": obj.ProfesionID,
		"perfiles.$.perfil":      obj.Perfil,
		// "perfiles.$.reputacion":          obj.Reputacion,
		"perfiles.$.fechaModificacion":   obj.FechaModificacion,
		"perfiles.$.usuarioModificacion": obj.UsuarioModificacion,
	}}
	err := repository.c.Update(query, change)

	return err
}

// DeletePerfilToProfesional elimina un perfil profesional
func (repository ProfesionalRepository) DeletePerfilToProfesional(profesionalID string, perfilID string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(profesionalID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(perfilID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	change := bson.M{"$pull": bson.M{"perfiles": bson.M{"_id": bson.ObjectIdHex(perfilID)}}}
	err := repository.c.UpdateId(bson.ObjectIdHex(profesionalID), change)

	return err
}

// ******** ESPECIALIDADES ********

// UpdateEspecialidadesToPerfil actualiza las especialidades de un perfil profesional
func (repository ProfesionalRepository) UpdateEspecialidadesToPerfil(profesionalID string, perfilID string, especialidades []string, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(profesionalID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(perfilID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	query := bson.M{"_id": bson.ObjectIdHex(profesionalID), "perfiles._id": bson.ObjectIdHex(perfilID)}
	change := bson.M{"$set": bson.M{"perfiles.$.especialidades": especialidades}}
	err := repository.c.Update(query, change)
	return err
}

// CreateMany agrega un array de objetos
func (repository ProfesionalRepository) CreateMany(objs []Profesional) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository ProfesionalRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository ProfesionalRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
