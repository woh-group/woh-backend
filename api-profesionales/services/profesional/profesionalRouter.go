package profesional

import (
	echo "github.com/labstack/echo/v4"
)

// Paths
const (
	pathPerfil                  = "/perfiles"
	pathHabilidades             = "/especialidades"
	pathEstudios                = "/estudios"
	pathEstudiosComplementarios = "/estudios-complementarios"
	pathExperiencia             = "/experiencias"
	pathIdiomas                 = "/idiomas"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = ProfesionalHandler{}

	// Profesional
	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/getByUsuarioId/:usuarioId", handler.GetByUsuarioID)
	g.GET("/count", handler.Count)
	g.POST("", handler.Create)
	g.PUT("/:id", handler.Update)
	g.DELETE("/:id", handler.Delete)
	g.PUT("/:id/idiomas", handler.UpdateIdiomas)

	// Perfiles
	g.POST("/:id/perfiles", handler.AddPerfilToProfesional)
	g.PUT("/:id/perfiles/:perfilId", handler.UpdatePerfilToProfesional)
	g.DELETE("/:id/perfiles/:perfilId", handler.DeletePerfilToProfesional)

	// Especialidades
	g.PUT("/:id/perfiles/:perfilId/especialidades", handler.UpdateEspecialidadesToPerfil)
}
