package profesional

import (
	"time"

	"gopkg.in/mgo.v2/bson"

	profesionalEstudio "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-estudio"
	profesionalExperiencia "gitlab.com/woh-group/woh-backend/api-profesionales/services/profesional-experiencia"
)

// Profesional entidad
type Profesional struct {
	ID                    bson.ObjectId       `bson:"_id,omitempty" json:"_id"`
	UsuarioID             string              `bson:"usuarioId" json:"usuarioId" validate:"required"`
	Persona               Persona             `bson:"persona" json:"persona"`
	EspecialidadPrincipal string              `bson:"especialidadPrincipal" json:"especialidadPrincipal"`
	Perfiles              []PerfilProfesional `bson:"perfiles" json:"perfiles,omitempty"`
	Idiomas               []string            `bson:"idiomas" json:"idiomas,omitempty"`
	Reputacion            float64             `bson:"reputacion" json:"reputacion"`
	Estado                bool                `bson:"estado" json:"estado"`
	FechaCreacion         time.Time           `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion       string              `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion     time.Time           `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion   string              `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// PerfilProfesional entidad
type PerfilProfesional struct {
	ID                      bson.ObjectId                        `bson:"_id,omitempty" json:"_id"`
	ProfesionID             int                                  `bson:"profesionId" json:"profesionId" validate:"required,min=1"`
	Perfil                  string                               `bson:"perfil" json:"perfil,omitempty"`
	Estudios                []profesionalEstudio.Estudio         `bson:"estudios" json:"estudios,omitempty"`
	EstudiosComplementarios []profesionalEstudio.Estudio         `bson:"estudiosComplementarios" json:"estudiosComplementarios,omitempty"`
	Experiencias            []profesionalExperiencia.Experiencia `bson:"experiencias" json:"experiencias,omitempty"`
	Especialidades          []string                             `bson:"especialidades" json:"especialidades,omitempty"`
	FechaCreacion           time.Time                            `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion         string                               `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion       time.Time                            `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion     string                               `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// Persona entidad
type Persona struct {
	Nombres   string `bson:"nombres" json:"nombres"`
	Apellidos string `bson:"apellidos" json:"apellidos"`
	UsuarioID string `bson:"usuarioId" json:"usuarioId,omitempty"`
}

// Usuario entidad
type Usuario struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"_id"`
}
