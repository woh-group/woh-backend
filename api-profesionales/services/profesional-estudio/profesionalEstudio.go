package profesionalestudio

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Estudio entidad
type Estudio struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	PerfilProfesionalID string        `bson:"perfilProfesionalId" json:"perfilProfesionalId" validate:"required"`
	TipoEstudioID       int           `bson:"tipoEstudioId" json:"tipoEstudioId" validate:"required"`
	Institucion         string        `bson:"institucion" json:"institucion" validate:"required"`
	NivelAcademicoID    int           `bson:"nivelAcademicoId" json:"nivelAcademicoId" validate:"required"`
	Estado              string        `bson:"estado" json:"estado" validate:"required"`
	Titulo              string        `bson:"titulo" json:"titulo" validate:"required"`
	FechaInicio         time.Time     `bson:"fechaInicio" json:"fechaInicio" validate:"required"`
	FechaFin            time.Time     `bson:"fechaFin" json:"fechaFin,omitempty"`
	TarjetaProfesional  string        `bson:"tarjetaProfesional" json:"tarjetaProfesional,omitempty"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
