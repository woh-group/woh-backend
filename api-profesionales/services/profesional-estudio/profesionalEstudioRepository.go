package profesionalestudio

import (
	"errors"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-profesionales/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "profesionalesEstudios"

// EstudioRepository objeto entidad
type EstudioRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

// Parametros
var typeFields = map[string]string{
	"perfilProfesionalId": dbUtil.TypeString,
	"tipoEstudioId":       dbUtil.TypeInteger,
	"institucion":         dbUtil.TypeString,
	"nivelAcademicoId":    dbUtil.TypeInteger,
	"estado":              dbUtil.TypeString,
	"titulo":              dbUtil.TypeString,
	"tarjetaProfesional":  dbUtil.TypeString,
}

func (repository *EstudioRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContext(CollectionName, &conectionDB)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository EstudioRepository) FindAll(c echo.Context) (*[]Estudio, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Estudio
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository EstudioRepository) FindByID(id string) (*Estudio, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Estudio
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// FindByPerfilProfesionalID devuelve un registro filtrado por perfilProfesionalId
func (repository EstudioRepository) FindByPerfilProfesionalID(PerfilProfesionalID string) (*[]Estudio, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Estudio
	err := repository.c.Find(bson.M{"perfilProfesionalId": PerfilProfesionalID}).All(&objs)
	return &objs, err
}

// Count devuelve la cantidad de registros
func (repository EstudioRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository EstudioRepository) Create(obj *Estudio, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository EstudioRepository) Update(id string, obj *Estudio, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"institucion":         obj.Institucion,
			"nivelAcademicoId":    obj.NivelAcademicoID,
			"titulo":              obj.Titulo,
			"estado":              obj.Estado,
			"fechaInicio":         obj.FechaInicio,
			"fechaFin":            obj.FechaFin,
			"tarjetaProfesional":  obj.TarjetaProfesional,
			"fechaModificacion":   obj.FechaModificacion,
			"usuarioModificacion": obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository EstudioRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.RemoveId(bson.ObjectIdHex(id))
}

// CreateMany agrega un array de objetos
func (repository EstudioRepository) CreateMany(objs []Estudio) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository EstudioRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository EstudioRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
