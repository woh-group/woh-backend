package profesionalexperiencia

import (
	"errors"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-profesionales/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "profesionalesExperiencias"

// ExperienciaRepository objeto entidad
type ExperienciaRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

// Parametros
var typeFields = map[string]string{
	"perfilProfesionalId": dbUtil.TypeString,
	"empresa":             dbUtil.TypeString,
	"cargo":               dbUtil.TypeString,
	"actualmente":         dbUtil.TypeBoolean,
}

func (repository *ExperienciaRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContext(CollectionName, &conectionDB)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository ExperienciaRepository) FindAll(c echo.Context) (*[]Experiencia, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Experiencia
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository ExperienciaRepository) FindByID(id string) (*Experiencia, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Experiencia
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// FindByPerfilProfesionalID devuelve un registro filtrado por perfilProfesionalId
func (repository ExperienciaRepository) FindByPerfilProfesionalID(PerfilProfesionalID string) (*[]Experiencia, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Experiencia
	err := repository.c.Find(bson.M{"perfilProfesionalId": PerfilProfesionalID}).All(&objs)
	return &objs, err
}

// Count devuelve la cantidad de registros
func (repository ExperienciaRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository ExperienciaRepository) Create(obj *Experiencia, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository ExperienciaRepository) Update(id string, obj *Experiencia, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"empresa":             obj.Empresa,
			"cargo":               obj.Cargo,
			"fechaInicio":         obj.FechaInicio,
			"fechaFin":            obj.FechaFin,
			"funciones":           obj.Funciones,
			"actuemente":          obj.Actualmente,
			"fechaModificacion":   obj.FechaModificacion,
			"usuarioModificacion": obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository ExperienciaRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.RemoveId(bson.ObjectIdHex(id))
}

// CreateMany agrega un array de objetos
func (repository ExperienciaRepository) CreateMany(objs []Experiencia) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository ExperienciaRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository ExperienciaRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
