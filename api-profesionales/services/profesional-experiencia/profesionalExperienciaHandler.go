package profesionalexperiencia

import (
	"net/http"
	"strings"

	echo "github.com/labstack/echo/v4"
	mgo "gopkg.in/mgo.v2"

	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaUtil "gitlab.com/woh-group/woh-backend/util-auditoria/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
)

// ExperienciaHandler objeto
type ExperienciaHandler struct{}

// Variables globales
const (
	ID                  = "id"
	PerfilProfesionalID = "perfilProfesionalId"
	APIName             = "profesionalesExperiencias"
)

// GetAll obtiene todos los registros
func (ExperienciaHandler) GetAll(c echo.Context) error {
	nombreMetodo := "GetAll"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var estudioRepository = ExperienciaRepository{}
	objs, err := estudioRepository.FindAll(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, objs)
}

// GetByID obtiene un registro por Id
func (ExperienciaHandler) GetByID(c echo.Context) error {
	nombreMetodo := "GetById"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)
	// Buscamos en la BD
	var estudioRepository = ExperienciaRepository{}
	obj, err := estudioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// GetByPerfilProfesionalID obtiene un registro por usuarioId
func (ExperienciaHandler) GetByPerfilProfesionalID(c echo.Context) error {
	nombreMetodo := "GetByPerfilProfesionalID"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(PerfilProfesionalID)

	// Buscamos en la BD
	var estudioRepository = ExperienciaRepository{}
	objs, err := estudioRepository.FindByPerfilProfesionalID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, &objs)
}

// Count obtiene la cantidad de registros
func (ExperienciaHandler) Count(c echo.Context) error {
	nombreMetodo := "Count"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var estudioRepository = ExperienciaRepository{}
	count, err := estudioRepository.Count(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, count)
}

// Create inserta un nuevo registro
func (ExperienciaHandler) Create(c echo.Context) error {
	nombreMetodo := "Create"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj Experiencia
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Agregamos a la BD
	var estudioRepository = ExperienciaRepository{}
	if err := estudioRepository.Create(&obj, username); err != nil {
		if strings.Contains(err.Error(), dbConstantes.CodeDuplicateKey) {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusConflict, err.Error())
			return c.JSON(http.StatusConflict, dbConstantes.ErrorDatabaseDuplicateKey)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

// Update actualiza un registro
func (ExperienciaHandler) Update(c echo.Context) error {
	nombreMetodo := "Update"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Experiencia
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var estudioRepository = ExperienciaRepository{}
	if err := estudioRepository.Update(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Delete elimina un registro
func (ExperienciaHandler) Delete(c echo.Context) error {
	nombreMetodo := "Delete"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Eliminamos de la BD
	var estudioRepository = ExperienciaRepository{}
	if err := estudioRepository.Delete(id); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}
