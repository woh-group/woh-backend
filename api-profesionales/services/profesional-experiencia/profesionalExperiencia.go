package profesionalexperiencia

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Experiencia entidad
type Experiencia struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	PerfilProfesionalID string        `bson:"perfilProfesionalId" json:"perfilProfesionalId" validate:"required"`
	Empresa             string        `bson:"empresa" json:"empresa" validate:"required"`
	Cargo               string        `bson:"cargo" json:"cargo" validate:"required"`
	FechaInicio         time.Time     `bson:"fechaInicio" json:"fechaInicio" validate:"required"`
	FechaFin            time.Time     `bson:"fechaFin" json:"fechaFin,omitempty"`
	Funciones           string        `bson:"funciones" json:"funciones" validate:"required"`
	Actualmente         bool          `bson:"actualmente" json:"actualmente"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
