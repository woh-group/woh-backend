module gitlab.com/woh-group/woh-backend/api-profesionales

require (
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/labstack/echo/v4 v4.1.5
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/woh-group/woh-backend/db-mongo v0.0.0-20190604010528-17c6582ed597
	gitlab.com/woh-group/woh-backend/util-api v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-auditoria v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-auth v0.0.0-20190519062713-f8cce431290b
	gitlab.com/woh-group/woh-backend/util-logger v0.0.0-20190621181500-9ad9ca5b56b1
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/sys v0.0.0-20190516110030-61b9204099cb // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190517183331-d88f79806bbd // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/go-playground/validator.v9 v9.28.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.2.2
)
