package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gopkg.in/mgo.v2/bson"

	rolService "gitlab.com/woh-group/woh-backend/api-usuarios/services/rol"
)

func main() {

	var personas, errPersonas = getRoles()
	if errPersonas != nil {
		fmt.Println("ERROR ->", errPersonas)
	}
	var rolRepository = rolService.RolRepository{}
	if errPersonas = rolRepository.CreateMany(personas); errPersonas != nil {
		fmt.Println("ERROR ->", errPersonas)
	} else {
		fmt.Println("ROLES cargados exitosamente!")
	}
}

func getRoles() ([]rolService.Rol, error) {
	objs, err := ioutil.ReadFile("jsons/roles.json")

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	var personas []rolService.Rol
	err = json.Unmarshal(objs, &personas)

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	for i := range personas {
		personas[i].ID = bson.NewObjectId()
		personas[i].FechaCreacion = time.Now()
		personas[i].UsuarioCreacion = "admin"
		personas[i].FechaModificacion = time.Time{}
	}

	return personas, nil
}
