package autenticacion

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = AutenticacionHandler{}

	g.POST("/login", handler.Login)
	g.GET("/logout", handler.Logout)
	g.GET("/getDataByToken", handler.GetDataByToken)
	g.POST("/refreshToken/:token", handler.RefreshToken)
}
