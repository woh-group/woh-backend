package autenticacion

// Autenticacion (entidad)
// swagger:model
type Autenticacion struct {
	// required: true
	Username string `bson:"username" json:"username" validate:"required"`
	// required: true
	Password string `bson:"password" json:"password" validate:"required"`
}
