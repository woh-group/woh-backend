package autenticacion

import (
	"net/http"

	echo "github.com/labstack/echo/v4"
	mgo "gopkg.in/mgo.v2"

	config "gitlab.com/woh-group/woh-backend/api-usuarios/config"
	usuarioService "gitlab.com/woh-group/woh-backend/api-usuarios/services/usuario"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
	securityUtil "gitlab.com/woh-group/woh-backend/util/security"
)

type AutenticacionHandler struct{}

// Variables globales
const (
	Username              = "username"
	APIName               = "autenticacion"
	LoginSuccess          = "Login exitoso"
	LoginFailed           = "Credenciales invalidas"
	ChangePasswordSuccess = "Cambio de contraseña exitoso"

	// Estados usuario
    LoginUsuarioInactivo = "Login invalido - usuario inactivo";
	LoginUsuarioPendiente = "Login exitoso - usuario pendiente por requisitos minimos";
    LoginUsuarioEnValidacion = "Login exitoso - usuario pendiente por aprobación";
    LoginUsuarioActivo = "Login exitoso - usuario activo";
    LoginUsuarioBloqueado = "Login exitoso - usuario bloqueado";
    LoginUsuarioEliminado = "Login invalido - usuario eliminado";
    LoginUsuarioAusente = "Login exitoso - usuario ausente";

	CollectionName = "autenticacion"
)

// Login metodo para iniciar session
func (AutenticacionHandler) Login(c echo.Context) error {
	nombreMetodo := "Login"
	defer c.Request().Body.Close()

	// Transformamos el body a la entidad
	var obj Autenticacion
	if err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, "NaN", APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil)

	// Validamos el logueo
	var usuarioRepository = usuarioService.UsuarioRepository{}
	usuario, err := usuarioRepository.FindByUsernameAndPassword(obj.Username, securityUtil.EncryptHMACSHA512(obj.Password, config.GetSecurityConfig().SecretKeyPassword))
	if err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusUnauthorized, LoginFailed)
			return c.JSON(http.StatusUnauthorized, LoginFailed)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Validamos los estados invalidos del usuario
	if usuario.EstadoID == usuarioService.EstadoUsuarioInactivo || usuario.EstadoID == usuarioService.EstadoUsuarioEliminado {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusUnauthorized, getDescriptionLoginUsuario(usuario.EstadoID))
		return c.JSON(http.StatusUnauthorized, LoginFailed)
	}

	dispositivo := c.Request().Header.Get(apiUtil.AppID)
	token := authUtil.CreateToken(usuario.ID.Hex(), usuario.Rol, usuario.Permisos, dispositivo)

	// Actualizamos la fecha del ultimo logueo
	if err := usuarioRepository.UpdateUltimaSession(usuario.ID, usuario); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, obj.Username, APIName, nombreMetodo, c.Path(), nil, http.StatusCreated, getDescriptionLoginUsuario(usuario.EstadoID))
	return c.JSON(http.StatusCreated, &token)
}

// Logout metodo para cerrar session
func (AutenticacionHandler) Logout(c echo.Context) error {
	return c.String(http.StatusOK, "Session cerrada con exito")
}

// GetDataByToken devuelve la data de un token
func (AutenticacionHandler) GetDataByToken(c echo.Context) error {

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	tokenInfo, err := authUtil.GetDataByToken(token)

	// Validamos el token
	if err != nil {
		return c.JSON(http.StatusUnauthorized, err.Error())
	}

	return c.JSON(http.StatusOK, &tokenInfo)
}

// RefreshToken refresca el token de autenticacion
func (AutenticacionHandler) RefreshToken(c echo.Context) error {
	// nombreMetodo := "RefreshToken"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	tokenRefresh := c.Param("token")

	// Validamos el refresh token
	valid, err := authUtil.ValidateToken(tokenRefresh)

	if err != nil || !valid {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	// Obtenemos los datos del access token
	accessToken, err := authUtil.GetDataByToken(token)

	if err != nil {
		return c.JSON(http.StatusUnauthorized, err.Error())
	}

	dispositivo := c.Request().Header.Get(apiUtil.AppID)
	newToken := authUtil.CreateToken(accessToken.Username, accessToken.Rol, accessToken.Permisos, dispositivo)

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusCreated, LoginSuccess)
	return c.JSON(http.StatusCreated, &newToken)
}

// getDescriptionLoginUsuario la descripcion del estado de un login
func getDescriptionLoginUsuario(estado int) string {
	switch estado {
    case usuarioService.EstadoUsuarioInactivo:
        return LoginUsuarioInactivo
    case usuarioService.EstadoUsuarioPendiente:
        return LoginUsuarioPendiente
    case usuarioService.EstadoUsuarioEnValidacion:
        return LoginUsuarioEnValidacion
    case usuarioService.EstadoUsuarioActivo:
        return LoginUsuarioActivo
    case usuarioService.EstadoUsuarioBloqueado:
        return LoginUsuarioBloqueado
    case usuarioService.EstadoUsuarioEliminado:
        return LoginUsuarioEliminado
    case usuarioService.EstadoUsuarioAusente:
        return LoginUsuarioAusente
    default:
        return "Estado Desconocido"
    }
}
