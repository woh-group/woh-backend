package rol

import (
	"errors"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	config "gitlab.com/woh-group/woh-backend/api-usuarios/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "roles"

// RolRepository objeto entidad
type RolRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

var indexUnique = mgo.Index{
	Key:    []string{"id"},
	Unique: true,
}

func (repository *RolRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContextWithIndex(CollectionName, &conectionDB, indexUnique)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository RolRepository) FindAll(params map[string]interface{}, sort string, pageNumber int, pageSize int) (*[]Rol, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Rol
	var err error

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository RolRepository) FindByID(id string) (*Rol, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Rol
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// Count devuelve la cantidad de registros
func (repository RolRepository) Count(params map[string]interface{}) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository RolRepository) Create(obj *Rol, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = time.Now()
	obj.UsuarioCreacion = usuarioCreacion
	obj.FechaModificacion = time.Time{}
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository RolRepository) Update(id string, obj *Rol, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	obj.FechaModificacion = time.Now()
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	// err := repository.c.UpdateId(obj.ID, &obj)
	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"id":                  obj.Id,
			"descripcion":         obj.Descripcion,
			"estado":              obj.Estado,
			"fechaModificacion":   obj.FechaModificacion,
			"usuarioModificacion": obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository RolRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	err := repository.c.RemoveId(bson.ObjectIdHex(id))
	return err
}

// CreateMany agrega un array de objetos
func (repository RolRepository) CreateMany(objs []Rol) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository RolRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository RolRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
