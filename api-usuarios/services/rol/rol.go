package rol

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Rol (entidad)
// swagger:model
type Rol struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Id                  int           `bson:"id" json:"id" validate:"required,min=1"`
	Descripcion         string        `bson:"descripcion" json:"descripcion" validate:"required"`
	Estado              bool          `bson:"estado" json:"estado"`
	FechaCreacion       time.Time     `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   time.Time     `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}
