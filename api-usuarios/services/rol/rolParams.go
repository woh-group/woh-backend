package rol

import (
	"strconv"

	echo "github.com/labstack/echo/v4"

	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// GetParametros obtiene los parametros
func GetParametros(c echo.Context) (map[string]interface{}, string, int, int) {

	// Obtenemos los parametros generales
	params := GetParametrosFiltro(c)

	// Obtenemos el parametro de ordenamiento
	sort := apiUtil.GetParametroSort(c)

	// Obtenemos los parametros de paginacion
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)
	return params, sort, pageNumber, pageSize
}

// GetParametrosFiltro obtiene los parametros para filtro
func GetParametrosFiltro(c echo.Context) map[string]interface{} {

	// Obtenemos los parametros
	params := make(map[string]interface{})

	id := c.QueryParam("id")
	descripcion := c.QueryParam("descripcion")
	estado := c.QueryParam("estado")

	if len(id) > 0 {
		params["id"], _ = strconv.Atoi(id)
	}
	if len(descripcion) > 0 {
		params["descripcion"] = descripcion
	}
	if len(estado) > 0 {
		params["estado"], _ = strconv.ParseBool(estado)
	}
	return params
}
