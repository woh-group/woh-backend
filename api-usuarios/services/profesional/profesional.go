package Profesional

import (
	"gopkg.in/mgo.v2/bson"
)

// Profesional entidad
type Profesional struct {
	ID                        bson.ObjectId          `bson:"_id,omitempty" json:"_id"`
	UsuarioID           string              `bson:"usuarioId" json:"usuarioId" validate:"required"`
}