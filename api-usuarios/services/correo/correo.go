package correo

// Correo entidad
type Correo struct {
	Destinatario    string            `bson:"destinatario" json:"destinatario" validate:"required"`
	RemitenteCorreo       string            `bson:"remitenteCorreo" json:"remitenteCorreo"`
	RemitenteNombre string            `bson:"remitenteNombre" json:"remitenteNombre"`
	Asunto          string            `bson:"asunto" json:"asunto" validate:"required"`
	Mensaje         string            `bson:"mensaje" json:"mensaje"`
	Palabras        map[string]string `bson:"palabras" json:"palabras"`
	NombrePlantilla string            `bson:"nombrePlantilla" json:"nombrePlantilla" validate:"required"`
}
