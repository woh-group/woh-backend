package usuario

import (
	"errors"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	config "gitlab.com/woh-group/woh-backend/api-usuarios/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "usuarios"

// UsuarioRepository objeto entidad
type UsuarioRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

var indexUnique = mgo.Index{
	Key:    []string{"username"},
	Unique: true,
}

var bsonSelect = bson.M{
	"_id":                        1,
	"username":                   1,
	"activationCode":             1,
	"cambiarContrasena":          1,
	"estadoId":                   1,
	"rol":                        1,
	"permisos":                   1,
	"aceptoRecibirPublicidad":    1,
	"versionTerminosCondiciones": 1,
	"fechaUltimaSession":         1,
	"persona":                    1,
	"fechaCreacion":              1,
	"usuarioCreacion":            1,
	"fechaModificacion":          1,
	"usuarioModificacion":        1,
}

// Parametros
var typeFields = map[string]string{
	"_id":                     dbUtil.TypeObjectID,
	"username":                dbUtil.TypeString,
	"estadoId":                dbUtil.TypeInteger,
	"rol":                     dbUtil.TypeInteger,
	"persona.tipoDocumentoId": dbUtil.TypeInteger,
	"persona.numeroDocumento": dbUtil.TypeString,
	"persona.nombres":         dbUtil.TypeString,
	"persona.apellidos":       dbUtil.TypeString,
	"persona.sexo":            dbUtil.TypeString,
	"persona.telefono":        dbUtil.TypeString,
	"persona.celular":         dbUtil.TypeString,
	"persona.fechaNacimiento": dbUtil.TypeDate,
	"persona.estadoCivil":     dbUtil.TypeString,
	"persona.correo":          dbUtil.TypeString,
	"persona.direccion":       dbUtil.TypeString,
	"persona.codigoPostal":    dbUtil.TypeString,
	"persona.paisId":          dbUtil.TypeString,
	"persona.departamentoId":  dbUtil.TypeString,
	"persona.ciudadId":        dbUtil.TypeString,
}

func (repository *UsuarioRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContextWithIndex(CollectionName, &conectionDB, indexUnique)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository UsuarioRepository) FindAll(c echo.Context) (*[]Usuario, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Usuario
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQueryWithSelect(repository.c, params, sort, pageNumber, pageSize, bsonSelect)
	err = query.All(&objs)
	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository UsuarioRepository) FindByID(id string) (*Usuario, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Usuario
	err := repository.c.FindId(bson.ObjectIdHex(id)).Select(bsonSelect).One(&obj)
	return &obj, err
}

// FindByUsername devuelve un registro filtrado por username
func (repository UsuarioRepository) FindByUsername(username string) (*Usuario, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var obj Usuario
	err := repository.c.Find(bson.M{"username": username}).Select(bsonSelect).One(&obj)
	return &obj, err
}

// Count devuelve la cantidad de registros
func (repository UsuarioRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository UsuarioRepository) Create(obj *Usuario, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	currentDate := time.Now()
	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = &currentDate
	obj.UsuarioCreacion = usuarioCreacion
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository UsuarioRepository) Update(id string, obj *Usuario, usuarioModificacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	currentDate := time.Now()
	obj.FechaModificacion = &currentDate
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"estadoId":                obj.EstadoID,
			"rol":                     obj.Rol,
			"permisos":                obj.Permisos,
			"password":                obj.Password,
			"activationCode":          obj.ActivationCode,
			"cambiarContrasena":       obj.CambiarContrasena,
			"aceptoRecibirPublicidad": obj.AceptoRecibirPublicidad,
			"persona.tipoDocumentoId": obj.Persona.TipoDocumentoID,
			"persona.numeroDocumento": obj.Persona.NumeroDocumento,
			"persona.nombres":         obj.Persona.Nombres,
			"persona.apellidos":       obj.Persona.Apellidos,
			"persona.sexo":            obj.Persona.Sexo,
			"persona.telefono":        obj.Persona.Telefono,
			"persona.celular":         obj.Persona.Celular,
			"persona.fechaNacimiento": obj.Persona.FechaNacimiento,
			"persona.estadoCivil":     obj.Persona.EstadoCivil,
			"persona.correo":          obj.Persona.Correo,
			"persona.direccion":       obj.Persona.Direccion,
			"persona.codigoPostal":    obj.Persona.CodigoPostal,
			"persona.paisId":          obj.Persona.PaisID,
			"persona.departamentoId":  obj.Persona.DepartamentoID,
			"persona.ciudadId":        obj.Persona.CiudadID,
			"fechaModificacion":       obj.FechaModificacion,
			"usuarioModificacion":     obj.UsuarioModificacion,
		}})
	return err
}

// UpdatePersona actualiza un registro
func (repository UsuarioRepository) UpdatePersona(id string, obj *Persona, usuarioModificacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	err := repository.c.UpdateId(bson.ObjectIdHex(id),
		bson.M{"$set": bson.M{
			"persona.tipoDocumentoId": obj.TipoDocumentoID,
			"persona.numeroDocumento": obj.NumeroDocumento,
			"persona.nombres":         obj.Nombres,
			"persona.apellidos":       obj.Apellidos,
			"persona.sexo":            obj.Sexo,
			"persona.telefono":        obj.Telefono,
			"persona.celular":         obj.Celular,
			"persona.fechaNacimiento": obj.FechaNacimiento,
			"persona.estadoCivil":     obj.EstadoCivil,
			"persona.correo":          obj.Correo,
			"persona.direccion":       obj.Direccion,
			"persona.codigoPostal":    obj.CodigoPostal,
			"persona.paisId":          obj.PaisID,
			"persona.departamentoId":  obj.DepartamentoID,
			"persona.ciudadId":        obj.CiudadID,
			"fechaModificacion":       time.Now(),
			"usuarioModificacion":     usuarioModificacion,
		}})
	return err
}

// UpdateUltimaSession actualiza la fecha de la ultima session
func (repository UsuarioRepository) UpdateUltimaSession(id bson.ObjectId, obj *Usuario) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	currentDate := time.Now()
	obj.FechaUltimaSession = &currentDate

	err := repository.c.UpdateId(id,
		bson.M{"$set": bson.M{
			"fechaUltimaSession": obj.FechaUltimaSession,
		}})
	return err
}

// UpdatePassword actualiza la contrasena de un usuario
func (repository UsuarioRepository) UpdatePassword(id bson.ObjectId, newPassword string, cambiarContrasena bool) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	err := repository.c.UpdateId(id,
		bson.M{"$set": bson.M{
			"password":          newPassword,
			"cambiarContrasena": cambiarContrasena,
		}})
	return err
}

// UpdateEstado actualiza el estado de un usuario
func (repository UsuarioRepository) UpdateEstado(id bson.ObjectId, estado int, usuarioModificacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	err := repository.c.UpdateId(id,
		bson.M{"$set": bson.M{
			"estadoId":            estado,
			"fechaModificacion":   time.Now(),
			"usuarioModificacion": usuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository UsuarioRepository) Delete(id string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	err := repository.c.RemoveId(bson.ObjectIdHex(id))
	return err
}

// FindByUsernameAndPassword devuelve un usuario por medio de login
func (repository UsuarioRepository) FindByUsernameAndPassword(username string, password string) (*Usuario, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var usuario Usuario
	err := repository.c.Find(bson.M{"username": username, "password": password}).Select(bsonSelect).One(&usuario)
	return &usuario, err
}

// FindByUsuarioIDAndActivationCode devuelve un usuario con codigo de activacion
func (repository UsuarioRepository) FindByUsuarioIDAndActivationCode(id string, activationCode string) (*Usuario, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var usuario Usuario
	err := repository.c.Find(bson.M{"_id": bson.ObjectIdHex(id), "activationCode": activationCode}).Select(bsonSelect).One(&usuario)
	return &usuario, err
}

// IsValidUsername valida si un username es valido
func (repository UsuarioRepository) IsValidUsername(username string) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	query := bson.M{"username": username, "estadoId": bson.M{"$ne": 1}}
	count, err := repository.c.Find(query).Count()
	return count, err
}

// CreateMany agrega un array de objetos
func (repository UsuarioRepository) CreateMany(objs []Usuario) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveAll elimina los registros de una coleccion
func (repository UsuarioRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository UsuarioRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}
