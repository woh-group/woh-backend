package usuario

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = UsuarioHandler{}

	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/getPersonaByUsuarioId/:id", handler.GetPersonaByUsuarioID)
	g.GET("/count", handler.Count)
	g.GET("/olvidoContrasena/:username", handler.GetForgetPassword)
	g.GET("/estadoC/:id", handler.EstadoCambiarContrasena)
	g.POST("", handler.Create)
	g.POST("/subirImagen", handler.UploadImageProfile)
	g.PUT("/:id", handler.Update)
	g.PUT("/updatePersona/:id", handler.UpdatePersona)
	g.DELETE("/:id", handler.Delete)
	g.GET("/isValidUsername/:username", handler.IsValidUsername)
	g.PATCH("/activarUsuario/:id/:activationCode", handler.ActivarUsuario)
	g.PATCH("/cambiarEstadoUsuario/:id/:estado", handler.CambiarEstadoUsuario)
	g.PATCH("/cambiarContrasena", handler.CambiarContrasena)
	g.PATCH("/restablecerContrasena/:id", handler.RestablecerContrasena)
}
