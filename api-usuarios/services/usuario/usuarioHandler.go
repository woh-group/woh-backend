package usuario

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"strconv"

	// "reflect"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	echo "github.com/labstack/echo/v4"
	mgo "gopkg.in/mgo.v2"

	config "gitlab.com/woh-group/woh-backend/api-usuarios/config"
	correoService "gitlab.com/woh-group/woh-backend/api-usuarios/services/correo"
	profesionalService "gitlab.com/woh-group/woh-backend/api-usuarios/services/profesional"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaUtil "gitlab.com/woh-group/woh-backend/util-auditoria/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
	randomUtil "gitlab.com/woh-group/woh-backend/util/random"
	securityUtil "gitlab.com/woh-group/woh-backend/util/security"
)

// UsuarioHandler objeto
type UsuarioHandler struct{}

// Variables globales
const (
	ID       = "id"
	Username = "username"
	APIName  = "usuarios"

	LoginFailed           = "Credenciales Invalidas"
	ChangePasswordSuccess = "Cambio de contraseña exitoso"
	NotFound              = "usuario not found"

	// Estados usuario
	EstadoUsuarioInactivo     = 1
	EstadoUsuarioPendiente    = 2
	EstadoUsuarioEnValidacion = 3
	EstadoUsuarioActivo       = 4
	EstadoUsuarioBloqueado    = 5
	EstadoUsuarioEliminado    = 6
	EstadoUsuarioAusente      = 7

	// Roles usuario
	RolUsuarioProfesional = 3
)

// GetAll obtiene todos los registros
// swagger:operation GET /usuarios usuarios getUsuarios
//
// Retorna una lista de usuarios
//
// .
// ---
// parameters:
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "page[number]"
//   in: path
//   description: "Número de pagina"
//   required: false
//   type: string
//
// - name: "page[size]"
//   in: path
//   description: "Cantidad de registros por pagina"
//   required: false
//   type: string
//
// - name: "sort"
//   in: path
//   description: "ASC: sort=nombre, DESC: sort=-nombre"
//   required: false
//   type: string
//
// - name: "username"
//   in: path
//   description: "Nombre de usuario"
//   required: false
//   type: string
//
// - name: "estadoId"
//   in: path
//   description: "Id del estado"
//   required: false
//   type: string
//
// - name: "personaId"
//   in: path
//   description: "Id de la persona"
//   required: false
//   type: string
//
// - name: "rol"
//   in: path
//   description: "Id del rol"
//   required: false
//   type: string
//
// Responses:
//   '200':
//     description: OK
//     schema:
//       type: array
//       items:
//         "$ref": "#/definitions/Usuario"
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '404':
//     description: usuario not found
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) GetAll(c echo.Context) error {
	nombreMetodo := "GetAll"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	objs, err := usuarioRepository.FindAll(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &objs, http.StatusOK, "")
	return c.JSON(http.StatusOK, objs)
}

// GetByID obtiene un registro por Id
// swagger:operation GET /usuarios/{id} usuarios getUsuario
//
// Retorna un usuario por id
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "id"
//   in: path
//   description: "Id del usuario"
//   required: true
//   type: string
//
// Responses:
//   '200':
//     description: OK
//     schema:
//         "$ref": "#/definitions/Usuario"
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '404':
//     description: usuario not found
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) GetByID(c echo.Context) error {
	nombreMetodo := "GetById"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// GetPersonaByUsuarioID obtiene la persona por usuarioId
func (UsuarioHandler) GetPersonaByUsuarioID(c echo.Context) error {
	nombreMetodo := "GetPersonaByUsuarioID"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj.Persona)
}

// GetByUsername obtiene un registro por username
func (UsuarioHandler) GetByUsername(c echo.Context) error {
	nombreMetodo := "GetByUsername"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(Username)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByUsername(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Count obtiene la cantidad de registros
// swagger:operation GET /usuarios/count usuarios countUsuarios
//
// Retorna la cantidad de usuarios
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// Responses:
//   '200':
//     description: OK
//     type: integer
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) Count(c echo.Context) error {
	nombreMetodo := "Count"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	count, err := usuarioRepository.Count(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, count)
}

// Create inserta un nuevo registro
// swagger:operation POST /usuarios usuarios addUsuario
//
// Agrega un usuario
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "Body"
//   in: Body
//   required: true
//   schema:
//         "$ref": "#/definitions/Usuario"
//
// Responses:
//   '201':
//     description: Created
//     schema:
//         "$ref": "#/definitions/Usuario"
//   '400':
//     description: Bad request
//     type: string
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '409':
//     description: Conflict - Ya existe un usuario con el mismo 'username'
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) Create(c echo.Context) error {
	nombreMetodo := "Create"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj Usuario
	if err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Logueamos
	objUsuarioLog := obj
	objUsuarioLog.Password = ""
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), objUsuarioLog)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Encriptamos el password
	obj.Password = securityUtil.EncryptHMACSHA512(obj.Password, config.GetSecurityConfig().SecretKeyPassword)
	obj.ActivationCode, _ = randomUtil.GenerateRandomString(randomUtil.TypeNumbers, 6)

	// Iniciamos el repositorio
	var usuarioRepository = UsuarioRepository{}

	// Agragamos el usuario
	isOldUser := false
	if err := usuarioRepository.Create(&obj, username); err != nil {

		// Validamos si el username ya existia
		if strings.Contains(err.Error(), dbConstantes.CodeDuplicateKey) {

			// Buscamos el usuario
			usuarioOld, errFindByUsername := usuarioRepository.FindByUsername(obj.Username)
			if errFindByUsername != nil {
				// Logueamos
				go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
				return c.NoContent(http.StatusInternalServerError)
			}

			// Validamos si tiene estado PENDIENTE
			if usuarioOld.EstadoID == EstadoUsuarioInactivo {
				// Asignamos el id al nuevo usuario
				obj.ID = usuarioOld.ID

				// Actualizamos el usuario
				if err := usuarioRepository.Update(obj.ID.Hex(), &obj, username); err != nil {
					if err == mgo.ErrNotFound {
						// Logueamos
						go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
						return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
					}
					// Logueamos
					go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
					return c.NoContent(http.StatusInternalServerError)
				}

				isOldUser = true
			} else {
				// Logueamos
				go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusConflict, err.Error())
				return c.JSON(http.StatusConflict, dbConstantes.ErrorDatabaseDuplicateKey)
			}
		}

		// Validamos si el usuario es viejo
		if !isOldUser {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
			return c.NoContent(http.StatusInternalServerError)
		}
	}

	// Quitamos el password
	obj.Password = ""

	// Enviamos el token de validacion al correo
	if obj.EstadoID == EstadoUsuarioInactivo {
		endpoint, statusCode, err := sendActivationMail(c, &obj.Persona, obj.ID.Hex(), obj.ActivationCode)
		if err != nil {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, endpoint, nil, http.StatusInternalServerError, "Ocurrio un error al enviar el correo: "+err.Error())
			return c.NoContent(http.StatusInternalServerError)
		}
		go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Envio de correo exitoso", endpoint, nil, statusCode, nil)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

func (UsuarioHandler) UploadImageProfile(c echo.Context) error {
	nombreMetodo := "UploadImageProfile"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	form, handler, err := c.Request().FormFile("uploadfile")
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Subimos la imagen
	defer form.Close()
	var Buf bytes.Buffer
	creds := credentials.NewStaticCredentials(config.GetConnectionConfig().Cloud.AWS.S3.AccessKeyId, config.GetConnectionConfig().Cloud.AWS.S3.SecretAccessKey, config.GetConnectionConfig().Cloud.AWS.S3.Token)

	if _, err := creds.Get(); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	if _, err := io.Copy(&Buf, form); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	path := "/perfil/" + handler.Filename

	// Set up config
	configAWS := &aws.Config{
		Region:      aws.String(config.GetConnectionConfig().Cloud.AWS.S3.S3Region),
		Credentials: creds,
	}

	sess := session.New(configAWS)
	uploader := s3manager.NewUploader(sess)
	result, err := uploader.Upload(&s3manager.UploadInput{
		Body:   &Buf,
		Bucket: aws.String(config.GetConnectionConfig().Cloud.AWS.S3.S3Bucket),
		Key:    aws.String(path),
		ContentType:        aws.String("image/jpeg"),
		ACL:    aws.String("public-read"),
	})

	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}
	Buf.Reset()

	// FIN METODO SUBIR IMAGEN

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, result, result, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), apiUtil.ConvertEntityToJSONString(result), http.StatusCreated, "")
	return c.NoContent(http.StatusCreated)
}

// Update actualiza un registro
// swagger:operation PUT /usuarios usuarios updateUsuario
//
// Actualiza un usuario
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "id"
//   in: path
//   description: "Id del usuario a actualizar"
//   required: true
//   type: string
//
// - name: "Body"
//   in: Body
//   required: true
//   schema:
//         "$ref": "#/definitions/Usuario"
//
// Responses:
//   '200':
//     description: Created
//     schema:
//         "$ref": "#/definitions/Usuario"
//   '400':
//     description: Bad request
//     type: string
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '404':
//     description: Usuario not found
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) Update(c echo.Context) error {
	nombreMetodo := "Update"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Usuario
	if err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var usuarioRepository = UsuarioRepository{}
	if err := usuarioRepository.Update(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	obj.Password = "*****"

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// UpdatePersona actualiza un registro
func (UsuarioHandler) UpdatePersona(c echo.Context) error {
	nombreMetodo := "UpdatePersona"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Persona
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		return c.NoContent(http.StatusBadRequest)
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var usuarioRepository = UsuarioRepository{}
	if err := usuarioRepository.UpdatePersona(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Delete elimina un registro
// swagger:operation DELETE /usuarios usuarios deleteUsuario
//
// Elimina un usuario
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "id"
//   in: path
//   description: "Id del usuario a actualizar"
//   required: true
//   type: string
//
// Responses:
//   '200':
//     description: Created
//     schema:
//         "$ref": "#/definitions/Usuario"
//   '204':
//     description: No se encontro el registro a eliminar
//     type: string
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) Delete(c echo.Context) error {
	nombreMetodo := "Delete"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Eliminamos de la BD
	var usuarioRepository = UsuarioRepository{}
	if err := usuarioRepository.Delete(id); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, NotFound)
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// IsValidUsername valida si un username es valido o no esta en uso
func (UsuarioHandler) IsValidUsername(c echo.Context) error {
	nombreMetodo := "IsValidUsername"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	username := c.Param("username")

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	count, err := usuarioRepository.IsValidUsername(username)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	isValid := true
	if count > 0 {
		isValid = false
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, isValid)
}

// ActivarUsuario activa un usuario
func (UsuarioHandler) ActivarUsuario(c echo.Context) error {
	nombreMetodo := "ActivarUsuario"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)
	activationCode := c.Param("activationCode")

	// Actualizamos la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByUsuarioIDAndActivationCode(id, activationCode)
	if err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Actualizamos el username para utilizarlo en los logs y auditoria
	username = obj.ID.Hex()
	estadoInicial := obj.EstadoID
	var estadoFinal int

	// Validamos el estado del usuario
	if obj.EstadoID != EstadoUsuarioInactivo {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, "El estado del usuario es diferente a INACTIVO")
		return c.NoContent(http.StatusNoContent)
	}

	// Obtenemos el estado final del usuario
	if obj.Rol == RolUsuarioProfesional {
		estadoFinal = EstadoUsuarioActivo
	} else {
		estadoFinal = EstadoUsuarioActivo
	}
	obj.EstadoID = estadoFinal

	// Activamos el usuario
	if err := usuarioRepository.UpdateEstado(obj.ID, estadoFinal, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, NotFound)
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Validamos el rol del usuario para agregar el profesional
	if obj.Rol == RolUsuarioProfesional {
		var profesional profesionalService.Profesional
		profesional.UsuarioID = obj.ID.Hex()
		statusCode, err := addProfesional(c, &profesional)
		fmt.Println("addProfesional", statusCode, err)
		if err != nil {

			// Validamos si el profesional ya existe
			if statusCode != 409 {
				// Hacemos rollback y desactivamos el usuario
				if err := usuarioRepository.UpdateEstado(obj.ID, EstadoUsuarioInactivo, username); err != nil {
					if err == mgo.ErrNotFound {
						// Logueamos
						go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, NotFound)
						return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
					}
					// Logueamos
					go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
					return c.NoContent(http.StatusInternalServerError)
				}

				// Logueamos
				go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
				return c.NoContent(http.StatusInternalServerError)
			}
		}
	}

	messageResponse := "Cambio de estado " + getDescriptionEstadoUsuario(estadoInicial) + " a " + getDescriptionEstadoUsuario(estadoFinal)

	// Auditamos
	go auditoriaUtil.AuditarWithComentario(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.UPDATE, messageResponse)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &obj, http.StatusCreated, messageResponse)
	return c.NoContent(http.StatusOK)
}

// CambiarContrasena metodo para cambiar la contrasena de un usuario
// swagger:operation POST /usuarios/cambiarContrasena usuarios cambiarContrasena
//
// Cambia la contraseña de un usuario
//
// .
// ---
// parameters:
// - name: "Authorization"
//   in: header
//   description: "Access token"
//   required: true
//   type: string
//
// - name: "Request-Id"
//   in: header
//   description: "Identificador único de la petición"
//   required: true
//   type: string
//   default: "123456789"
//
// - name: "App-Id"
//   in: header
//   description: "Identificador del consumidor"
//   required: true
//   type: string
//   default: "123-swagger"
//
// - name: "Body"
//   in: Body
//   required: true
//   schema:
//         "$ref": "#/definitions/CambioContrasena"
//
// Responses:
//   '200':
//     description: OK
//     type: string
//   '400':
//     description: Bad request
//     type: string
//   '401':
//     description: Token invalido
//     type: string
//   '403':
//     description: Acceso denegado o permisos insuficientes
//     type: string
//   '500':
//     description: Internal server error
//     type: string
func (UsuarioHandler) CambiarContrasena(c echo.Context) error {
	nombreMetodo := "CambiarContrasena"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Transformamos el body a la entidad
	var obj CambioContrasena
	if err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Validamos el logueo
	var usuarioRepository = UsuarioRepository{}
	fmt.Println("obj.Username: ", obj.Username)
	fmt.Println("obj.OldPassword: ", obj.OldPassword)
	fmt.Println("clave: ", securityUtil.EncryptHMACSHA512(obj.OldPassword, config.GetSecurityConfig().SecretKeyPassword))
	usuario, err := usuarioRepository.FindByUsernameAndPassword(obj.Username, securityUtil.EncryptHMACSHA512(obj.OldPassword, config.GetSecurityConfig().SecretKeyPassword))
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	usuario.Password = "*****"
	fmt.Println("obj.NewPassword: ", obj.NewPassword)
	// Actualizamos la contrasena
	if err := usuarioRepository.UpdatePassword(usuario.ID, securityUtil.EncryptHMACSHA512(obj.NewPassword, config.GetSecurityConfig().SecretKeyPassword), false); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &usuario, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.AuditarWithComentario(CollectionName, username, c.Request().Header, usuario.ID, &usuario, auditoriaUtil.UPDATE, "Cambio de Contraseña")

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &usuario, http.StatusCreated, ChangePasswordSuccess)
	return c.NoContent(http.StatusOK)
}

func (UsuarioHandler) RestablecerContrasena(c echo.Context) error {
	nombreMetodo := "RestablecerContrasena"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	usuario, err := usuarioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	usuario.Password = "*****"

	// Actualizamos la contrasena
	if err := usuarioRepository.UpdatePassword(usuario.ID, securityUtil.EncryptHMACSHA512("123456", config.GetSecurityConfig().SecretKeyPassword), false); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &usuario, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.AuditarWithComentario(CollectionName, username, c.Request().Header, usuario.ID, &usuario, auditoriaUtil.UPDATE, "Restablecer Contraseña")

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &usuario, http.StatusCreated, ChangePasswordSuccess)
	return c.NoContent(http.StatusOK)
}

func (UsuarioHandler) CambiarEstadoUsuario(c echo.Context) error {
	nombreMetodo := "CambiarEstadoUsuario"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)
	estado, _ := strconv.Atoi(c.Param("estado"))

	// Validamos los parametros
	if len(id) == 0 || (estado <= 0 || estado >= 7) {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, "Parametros invalidos")
		return c.NoContent(http.StatusBadRequest)
	}

	// Actualizamos la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	if err := usuarioRepository.UpdateEstado(obj.ID, estado, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, NotFound)
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	messageResponse := "Cambio de estado " + getDescriptionEstadoUsuario(obj.EstadoID) + " a " + getDescriptionEstadoUsuario(estado)
	obj.EstadoID = estado

	// Auditamos
	go auditoriaUtil.AuditarWithComentario(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.UPDATE, messageResponse)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), &obj, http.StatusCreated, messageResponse)
	return c.NoContent(http.StatusOK)
}

func (UsuarioHandler) EstadoCambiarContrasena(c echo.Context) error {
	nombreMetodo := "EstadoCambiarContrasena"

	defer c.Request().Body.Close()
	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}
	// Obtenemos los parametros
	id := c.Param("id")

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, id, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, id, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, id, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, obj.CambiarContrasena)
}

// ******** OTROS ********

// addProfesional agrega un profesional
func addProfesional(c echo.Context, profesional *profesionalService.Profesional) (int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPIProfesionales
	fmt.Println("endpoint", endpoint)

	return apiUtil.GetResponse(apiUtil.Post, endpoint, apiUtil.GetEndpointConfig().TimeoutAPIProfesionales, c.Request().Header, &profesional, &profesional)
}

// sendActivationMail envia un email
func sendActivationMail(c echo.Context, persona *Persona, usuarioID string, activationCode string) (string, int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPICorreos

	url := fmt.Sprintf("%s%s", endpoint, "/send")
	var correo correoService.Correo
	correo.Destinatario = persona.Correo
	correo.Asunto = config.GetConstantConfig().Email.EmailActivarUsuario.Asunto

	palabras := make(map[string]string)
	palabras["NOMBRES"] = persona.Nombres
	palabras["APELLIDOS"] = persona.Apellidos
	palabras["URL_ACTIVACION"] = config.GetConstantConfig().URLBase + "/usuario/activacion/" + usuarioID + "/" + activationCode

	correo.Palabras = palabras
	correo.NombrePlantilla = config.GetConstantConfig().Email.EmailActivarUsuario.Plantilla

	statusCode, err := apiUtil.GetResponse(apiUtil.Post, url, apiUtil.GetEndpointConfig().TimeoutAPICorreos, c.Request().Header, &correo, nil)
	return url, statusCode, err
}

// sendRecoveryPasswordMail envia un email
func sendRecoveryPasswordMail(c echo.Context, persona *Persona, usuarioID string, passwordTemporal string) (string, int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPICorreos

	url := fmt.Sprintf("%s%s", endpoint, "/send")
	var correo correoService.Correo
	correo.Destinatario = persona.Correo
	correo.Asunto = config.GetConstantConfig().Email.EmailRestablecerContrasena.Asunto

	palabras := make(map[string]string)
	palabras["NOMBRES"] = persona.Nombres
	palabras["APELLIDOS"] = persona.Apellidos
	palabras["PASSWORD_TEMPORAL"] = passwordTemporal
	palabras["URL_RESTABLECER"] = config.GetConstantConfig().URLBase + "/usuario/restablecerContrasena/" + usuarioID

	correo.Palabras = palabras
	correo.NombrePlantilla = config.GetConstantConfig().Email.EmailRestablecerContrasena.Plantilla

	statusCode, err := apiUtil.GetResponse(apiUtil.Post, url, apiUtil.GetEndpointConfig().TimeoutAPICorreos, c.Request().Header, &correo, nil)
	return url, statusCode, err
}

// getDescriptionEstadoUsuario devuelve descripcion del estado
func getDescriptionEstadoUsuario(estado int) string {
	switch estado {
	case EstadoUsuarioInactivo:
		return "Inactivo"
	case EstadoUsuarioPendiente:
		return "Pendiente"
	case EstadoUsuarioEnValidacion:
		return "En Validacion"
	case EstadoUsuarioActivo:
		return "Activo"
	case EstadoUsuarioBloqueado:
		return "Bloqueado"
	case EstadoUsuarioEliminado:
		return "Eliminado"
	case EstadoUsuarioAusente:
		return "Ausente"
	default:
		return "Estado Desconocido"
	}
}

func (UsuarioHandler) GetForgetPassword(c echo.Context) error {
	nombreMetodo := "GetForgetPassword"
	defer c.Request().Body.Close()
	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}
	// Obtenemos los parametros
	username := c.Param("username")

	// Buscamos en la BD
	var usuarioRepository = UsuarioRepository{}
	obj, err := usuarioRepository.FindByUsername(username)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Encriptamos el password
	var cadenaRandom, _ = randomUtil.GenerateRandomString(randomUtil.TypeLettersAndNumbers, 30)
	obj.Password = securityUtil.EncryptHMACSHA512(cadenaRandom, config.GetSecurityConfig().SecretKeyPassword)
	obj.CambiarContrasena = true
	// Actualizamos la contraseña por la nueva temporal
	if err := usuarioRepository.Update(obj.ID.Hex(), obj, username); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Enviamos el password temporal al correo
	if obj.CambiarContrasena {
		endpoint, statusCode, err := sendRecoveryPasswordMail(c, &obj.Persona, obj.ID.Hex(), cadenaRandom)
		if err != nil {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, endpoint, nil, http.StatusInternalServerError, "Ocurrio un error al enviar el correo: "+err.Error())
			return c.NoContent(http.StatusInternalServerError)
		}
		go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Envio de correo exitoso", endpoint, nil, statusCode, nil)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusOK, &obj)
}
