package usuario

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Usuario (entidad)
// swagger:model
type Usuario struct {
	ID                         bson.ObjectId   `bson:"_id,omitempty" json:"_id"`
	Username                   string          `bson:"username" json:"username" validate:"eqfield=Persona.Correo"`
	Password                   string          `bson:"password" json:"password"`
	ActivationCode             string          `bson:"activationCode" json:"activationCode"`
	CambiarContrasena          bool            `bson:"cambiarContrasena" json:"cambiarContrasena"`
	EstadoID                   int             `bson:"estadoId" json:"estadoId" validate:"required,min=1,max=7"`
	Rol                        int             `bson:"rol" json:"rol" validate:"required,min=1"`
	Permisos                   map[string]bool `bson:"permisos" json:"permisos"`
	AceptoRecibirPublicidad    bool            `bson:"aceptoRecibirPublicidad" json:"aceptoRecibirPublicidad"`
	VersionTerminosCondiciones float32         `bson:"versionTerminosCondiciones" json:"versionTerminosCondiciones" validate:"required,min=1"`
	FechaUltimaSession         *time.Time      `bson:"fechaUltimaSession" json:"fechaUltimaSession,omitempty"`
	Persona                    Persona         `bson:"persona" json:"persona,omitempty" validate:"structonly"`
	FechaCreacion              *time.Time      `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion            string          `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion          *time.Time      `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion        string          `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// Persona entidad
type Persona struct {
	TipoDocumentoID int        `bson:"tipoDocumentoId" json:"tipoDocumentoId,omitempty"`
	NumeroDocumento string     `bson:"numeroDocumento" json:"numeroDocumento,omitempty"`
	Nombres         string     `bson:"nombres" json:"nombres,omitempty" validate:"required"`
	Apellidos       string     `bson:"apellidos" json:"apellidos,omitempty"`
	Sexo            string     `bson:"sexo" json:"sexo,omitempty" validate:"required"`
	Telefono        string     `bson:"telefono" json:"telefono,omitempty"`
	Celular         string     `bson:"celular" json:"celular,omitempty"`
	FechaNacimiento *time.Time `bson:"fechaNacimiento" json:"fechaNacimiento,omitempty" validate:"required"`
	EstadoCivil     string     `bson:"estadoCivil" json:"estadoCivil,omitempty" validate:"required"`
	Correo          string     `bson:"correo" json:"correo,omitempty" validate:"required,email"`
	Direccion       string     `bson:"direccion" json:"direccion,omitempty"`
	CodigoPostal    string     `bson:"codigoPostal" json:"codigoPostal,omitempty"`
	PaisID          string     `bson:"paisId" json:"paisId,omitempty"`
	DepartamentoID  string     `bson:"departamentoId" json:"departamentoId,omitempty"`
	CiudadID        string     `bson:"ciudadId" json:"ciudadId,omitempty"`
}

// CambioContrasena (entidad)
// swagger:model
type CambioContrasena struct {
	Username    string `bson:"username" json:"username" validate:"required"`
	OldPassword string `bson:"oldPassword" json:"oldPassword" validate:"required"`
	NewPassword string `bson:"newPassword" json:"newPassword" validate:"required"`
}
