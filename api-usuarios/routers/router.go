package routers

import (
	echo "github.com/labstack/echo/v4"

	autenticacionService "gitlab.com/woh-group/woh-backend/api-usuarios/services/autenticacion"
	rolService "gitlab.com/woh-group/woh-backend/api-usuarios/services/rol"
	usuarioService "gitlab.com/woh-group/woh-backend/api-usuarios/services/usuario"
)

// PATH de la api
const PATH = "/api/usuarios"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {

	// create groups
	autenticacionGroup := e.Group(PATH + "/autenticacion")
	rolGroup := e.Group(PATH + "/roles")
	usuarioGroup := e.Group(PATH + "/usuarios")

	autenticacionService.SetRouters(autenticacionGroup)
	rolService.SetRouters(rolGroup)
	usuarioService.SetRouters(usuarioGroup)
}
