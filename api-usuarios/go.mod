module gitlab.com/woh-group/woh-backend/api-usuarios

require (
	github.com/aws/aws-sdk-go v1.20.5
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-openapi/spec v0.19.2 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.6
	github.com/mailru/easyjson v0.0.0-20190620125010-da37f6c1e481 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/swaggo/gin-swagger v1.1.0 // indirect
	github.com/swaggo/swag v1.5.1
	github.com/ugorji/go v1.1.5-pre // indirect
	gitlab.com/woh-group/woh-backend v0.0.0-20190621181500-9ad9ca5b56b1 // indirect
	gitlab.com/woh-group/woh-backend/api-comunes v0.0.0-20190621172216-c77502aa4710 // indirect
	gitlab.com/woh-group/woh-backend/api-personas v0.0.0-20190510012836-c713f56c59f0
	gitlab.com/woh-group/woh-backend/api-preguntas v0.0.0-20190621172216-c77502aa4710 // indirect
	gitlab.com/woh-group/woh-backend/api-profesionales v0.0.0-20190621172216-c77502aa4710 // indirect
	gitlab.com/woh-group/woh-backend/db-mongo v0.0.0-20190621172216-c77502aa4710
	gitlab.com/woh-group/woh-backend/util v0.0.0-20190621172216-c77502aa4710
	gitlab.com/woh-group/woh-backend/util-api v0.0.0-20190621172216-c77502aa4710
	gitlab.com/woh-group/woh-backend/util-auditoria v0.0.0-20190621172216-c77502aa4710
	gitlab.com/woh-group/woh-backend/util-auth v0.0.0-20190621172216-c77502aa4710
	gitlab.com/woh-group/woh-backend/util-logger v0.0.0-20190621181500-9ad9ca5b56b1
	golang.org/x/crypto v0.0.0-20190618222545-ea8f1a30c443 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/sys v0.0.0-20190621153339-943d5127bdb0 // indirect
	golang.org/x/tools v0.0.0-20190620191750-1fa568393b23 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/validator.v2 v2.0.0-20180514200540-135c24b11c19
	gopkg.in/yaml.v2 v2.2.2
)
