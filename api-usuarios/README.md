# Descripción

Proyecto api de seguridad y autenticacion

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-usuarios/...

## Crear Modulo

export GO111MODULE=auto
export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-usuarios

## Swagger generar swagger.json

https://goswagger.io/

Escanear los modelos

swagger generate spec -o ./swagger.json --scan-models

Probar online

swagger serve -F=swagger swagger.json

## Docker

docker build -t api-usuarios .

docker build --no-cache -t api-usuarios .

docker run -it api-usuarios bash

### Docker Linux

docker run -it --name api-usuarios -p 3001:3001 api-usuarios

### Docker Windows

winpty docker run -it --name api-usuarios -p 3001:3001 api-usuarios

### Docker con enviroment

#### En Linux

docker run -it --name api-usuarios -p 3001:3001 -v ${WOH_CONFIG_PATH}:/config -e WOH_CONFIG_PATH=/config api-usuarios
docker run -it --name api-usuarios -p 3001:3001 -v /home/ubuntu/woh/_CONFIG_FILES:/config -e WOH_CONFIG_PATH=/config api-usuarios

#### En Windows

docker run -it --name api-usuarios -p 3001:3001 -v "%WOH_CONFIG_PATH%":/config -e WOH_CONFIG_PATH=/config api-usuarios


field[contains] = Contiene
field[startswith] = Comienza con
field[endswith] = Termina con
field[ne] = No es igual
field[gt] = Mayor que
field[lt] = Menor que
field[gte] = Mayor o igual que
field[lte] = Menor o igual que

Between
field[start] = Campo inicial
field[end] = Campo final