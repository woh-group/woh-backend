package util

import (
	"fmt"
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

// Constantes
var (
	endpointConfigFile = "endpointConfig.yaml"
)

// EndpointConfig entity
type EndpointConfig struct {
	// Endpoints Api Comunes
	EndpointAPIPaises string `yaml:"endpointApiPaises" json:"endpointApiPaises"`
	TimeoutAPIPaises  int    `yaml:"timeoutApiPaises" json:"timeoutApiPaises"`

	EndpointAPIDepartamentos string `yaml:"endpointApiDepartamentos" json:"endpointApiDepartamentos"`
	TimeoutAPIDepartamentos  int    `yaml:"timeoutApiDepartamentos" json:"timeoutApiDepartamentos"`

	EndpointAPICiudades string `yaml:"endpointApiCiudades" json:"endpointApiCiudades"`
	TimeoutAPICiudades  int    `yaml:"timeoutApiCiudades" json:"timeoutApiCiudades"`

	EndpointAPITiposDocumento string `yaml:"endpointApiTiposDocumento" json:"endpointApiTiposDocumento"`
	TimeoutAPITiposDocumento  int    `yaml:"timeoutApiTiposDocumento" json:"timeoutApiTiposDocumento"`

	EndpointAPINivelesAcademicos string `yaml:"endpointApiNivelesAcademicos" json:"endpointApiNivelesAcademicos"`
	TimeoutAPINivelesAcademicos  int    `yaml:"timeoutApiNivelesAcademicos" json:"timeoutApiNivelesAcademicos"`

	EndpointAPIProfesiones string `yaml:"endpointApiProfesiones" json:"endpointApiProfesiones"`
	TimeoutAPIProfesiones  int    `yaml:"timeoutApiProfesiones" json:"timeoutApiProfesiones"`

	EndpointAPICorreos string `yaml:"endpointApiCorreos" json:"endpointApiCorreos"`
	TimeoutAPICorreos  int    `yaml:"timeoutApiCorreos" json:"timeoutApiCorreos"`

	// Endpoints Api Usuarios
	EndpointAPIRoles string `yaml:"endpointApiRoles" json:"endpointApiRoles"`
	TimeoutAPIRoles  int    `yaml:"timeoutApiRoles" json:"timeoutApiRoles"`

	EndpointAPIAutenticacion string `yaml:"endpointApiAutenticacion" json:"endpointApiAutenticacion"`
	TimeoutAPIAutenticacion  int    `yaml:"timeoutApiAutenticacion" json:"timeoutApiAutenticacion"`

	EndpointAPIUsuarios string `yaml:"endpointApiUsuarios" json:"endpointApiUsuarios"`
	TimeoutAPIUsuarios  int    `yaml:"timeoutApiUsuarios" json:"timeoutApiUsuarios"`

	// Endpoints Api Profesionales
	EndpointAPIProfesionales string `yaml:"endpointApiProfesionales" json:"endpointApiProfesionales"`
	TimeoutAPIProfesionales  int    `yaml:"timeoutApiProfesionales" json:"timeoutApiProfesionales"`

	EndpointAPIProfesionalesEstudios string `yaml:"endpointApiProfesionalesEstudios" json:"endpointApiProfesionalesEstudios"`
	TimeoutAPIProfesionalesEstudios  int    `yaml:"timeoutApiProfesionalesEstudios" json:"timeoutApiProfesionalesEstudios"`

	EndpointAPIProfesionalesExperiencias string `yaml:"endpointApiProfesionalesExperiencias" json:"endpointApiProfesionalesExperiencias"`
	TimeoutAPIProfesionalesExperiencias  int    `yaml:"timeoutApiProfesionalesExperiencias" json:"timeoutApiProfesionalesExperiencias"`

	EndpointAPIEspecialidades string `yaml:"endpointApiEspecialidades" json:"endpointApiEspecialidades"`
	TimeoutAPIEspecialidades  int    `yaml:"timeoutApiEspecialidades" json:"timeoutApiEspecialidades"`

	// Endpoints Api Preguntas
	EndpointAPIPreguntas string `yaml:"endpointApiPreguntas" json:"endpointApiPreguntas"`
	TimeoutAPIPreguntas  int    `yaml:"timeoutApiPreguntas" json:"timeoutApiPreguntas"`

	EndpointAPIRespuestas string `yaml:"endpointApiRespuestas" json:"endpointApiRespuestas"`
	TimeoutAPIRespuestas  int    `yaml:"timeoutApiRespuestas" json:"timeoutApiRespuestas"`
}

var endpointConfig EndpointConfig

// GetEndpointConfig obtiene la configuracion de los endpoints
func GetEndpointConfig() *EndpointConfig {
	// Validamos si la config esta vacia
	if (EndpointConfig{}) == endpointConfig {
		if err := readEndpointConfigFile(); err != nil {
			panic(err)
		}
	}
	return &endpointConfig
}

// readEndpointConfigFile read endpoint config file
func readEndpointConfigFile() error {

	configPath := getPath()
	fileContent, err := ioutil.ReadFile(configPath + "/" + endpointConfigFile)
	if err != nil {
		fmt.Printf("Error read config file: %v\n", err)
		return err
	}

	// expand environment variables
	fileContent = []byte(os.ExpandEnv(string(fileContent)))
	if err := yaml.Unmarshal(fileContent, &endpointConfig); err != nil {
		fmt.Printf("Error Unmarshal: %v\n", err)
		return err
	}

	// fmt.Printf("Load endpoint config: %v\n", endpointConfig)
	return nil
}

func getPath() string {
	// Obtenemos la ruta de la env sino devolvemos la ruta por defecto
	envConfigPath := os.Getenv("WOH_CONFIG_PATH")
	if len(envConfigPath) > 0 {
		return envConfigPath
	}
	return "config"
}
