module gitlab.com/woh-group/woh-backend/util-api

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/labstack/echo/v4 v4.0.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613 // indirect
	golang.org/x/sys v0.0.0-20190203050204-7ae0202eb74c // indirect
	gopkg.in/go-playground/validator.v9 v9.26.0
	gopkg.in/yaml.v2 v2.2.2
)
