package categoria

var (
	categorias    *[]Categoria
	categoriasMap = make(map[string]Categoria)
)

// GetHijos obtiene los hijos de una categoria
func GetHijos(categoria *Categoria, categoriasHijos []CategoriaBasic, categorias *[]Categoria) []CategoriaBasic {
	if categoria != nil && len(*categorias) > 0 {

		if categoriasHijos == nil {
			categoriasHijos = []CategoriaBasic{}
		}

		for _, v := range *categorias {
			if v.CategoriaPadreID == categoria.Id {

				categoriaHijo := CategoriaBasic{
					ID:               v.ID,
					Id:               v.Id,
					Descripcion:      v.Descripcion,
					CategoriaPadreID: v.CategoriaPadreID,
				}
				categoriasHijos = append(categoriasHijos, categoriaHijo)
				return GetHijos(&v, categoriasHijos, categorias)
			}
		}
	}
	return categoriasHijos
}

// GetPadres obtiene los padres de una categoria
func GetPadres(categoria *Categoria, categoriasPadres []CategoriaBasic, categorias *[]Categoria) []CategoriaBasic {
	if categoria != nil && len(*categorias) > 0 {

		if categoriasPadres == nil {
			categoriasPadres = []CategoriaBasic{}
		}

		for _, v := range *categorias {
			if v.Id == categoria.CategoriaPadreID {

				categoriaPadre := CategoriaBasic{
					ID:               v.ID,
					Id:               v.Id,
					Descripcion:      v.Descripcion,
					CategoriaPadreID: v.CategoriaPadreID,
				}
				categoriasPadres = append(categoriasPadres, categoriaPadre)
				return GetPadres(&v, categoriasPadres, categorias)
			}
		}
	}
	return categoriasPadres
}
