package pregunta

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = PreguntaHandler{}

	g.GET("", handler.GetAll)
	g.GET("/:id", handler.GetByID)
	g.GET("/count", handler.Count)
	g.POST("", handler.Create)
	g.PUT("/:id", handler.Update)
	g.DELETE("/:id", handler.Delete)
	g.PATCH("/:id/increase", handler.Increase)
	
	// Respuestas
	g.POST("/:preguntaId/respuestas", handler.CreateRespuesta)
	g.PUT("/:preguntaId/respuestas/:id", handler.UpdateRespuesta)
	g.DELETE("/:preguntaId/respuestas/:id", handler.DeleteRespuesta)
	g.PATCH("/:preguntaId/respuestas/:id/increase", handler.IncreaseRespuesta)

	// Reports
	g.GET("/reports/preguntasAndRespuestasByYear/:year", handler.GetReportPreguntasAndRespuestasByYear)
}
