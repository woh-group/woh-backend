package pregunta

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"

	echo "github.com/labstack/echo/v4"
	mgo "gopkg.in/mgo.v2"

	config "gitlab.com/woh-group/woh-backend/api-preguntas/config"
	correoService "gitlab.com/woh-group/woh-backend/api-preguntas/services/correo"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaUtil "gitlab.com/woh-group/woh-backend/util-auditoria/util"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
)

// PreguntaHandler objeto
type PreguntaHandler struct{}

// Variables globales
const (
	ID         = "id"
	preguntaID = "preguntaId"
	APIName    = "preguntas"
)

// GetAll obtiene todos los registros
func (PreguntaHandler) GetAll(c echo.Context) error {
	nombreMetodo := "GetAll"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var preguntaRepository = PreguntaRepository{}
	objs, err := preguntaRepository.FindAll(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Mapeamos los datos de la persona en las respuestas
	for _, obj := range *objs {
		asignPersonasToRespuestas(obj.Respuestas, c)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, objs)
}

// GetByID obtiene un registro por Id
func (PreguntaHandler) GetByID(c echo.Context) error {
	nombreMetodo := "GetById"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Buscamos en la BD
	var preguntaRepository = PreguntaRepository{}
	obj, err := preguntaRepository.FindByID(id)
	if err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Mapeamos los datos de la persona en las respuestas
	asignPersonasToRespuestas(obj.Respuestas, c)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Count obtiene la cantidad de registros
func (PreguntaHandler) Count(c echo.Context) error {
	nombreMetodo := "Count"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Buscamos en la BD
	var preguntaRepository = PreguntaRepository{}
	count, err := preguntaRepository.Count(c)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.JSON(http.StatusOK, count)
}

// Create inserta un nuevo registro
func (PreguntaHandler) Create(c echo.Context) error {
	nombreMetodo := "Create"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj Pregunta
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Agregamos a la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.Create(&obj, username); err != nil {
		if strings.Contains(err.Error(), dbConstantes.CodeDuplicateKey) {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusConflict, err.Error())
			return c.JSON(http.StatusConflict, dbConstantes.ErrorDatabaseDuplicateKey)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

// Update actualiza un registro
func (PreguntaHandler) Update(c echo.Context) error {
	nombreMetodo := "Update"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos en requestBody en la entidad
	var obj Pregunta
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Obtenemos los parametros
	id := c.Param(ID)

	// Actualizamos la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.Update(id, &obj, username); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// Delete elimina un registro
func (PreguntaHandler) Delete(c echo.Context) error {
	nombreMetodo := "Delete"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)

	// Eliminamos de la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.Delete(id); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// Increase incrementa un campo
func (PreguntaHandler) Increase(c echo.Context) error {
	nombreMetodo := "Increase"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	// go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	id := c.Param(ID)
	field := c.QueryParam("field")
	value, _ := strconv.Atoi(c.QueryParam("value"))

	// Incrementamos el registro en la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.Increase(id, field, value); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	// go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// ******** RESPUESTAS ********

// CreateRespuesta inserta un nuevo registro
func (PreguntaHandler) CreateRespuesta(c echo.Context) error {
	nombreMetodo := "CreateRespuesta"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Transformamos el body a la entidad
	var obj Respuesta
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Obtenemos los parametros
	preguntaID := c.Param(preguntaID)

	// Agregamos a la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.CreateRespuesta(preguntaID, &obj, username); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, obj.ID, &obj, auditoriaUtil.ADD)

	// Obtenemos la pregunta
	pregunta, err := preguntaRepository.FindByID(preguntaID)
	// fmt.Println("pregunta", pregunta, err)
	if err == nil {

		// Obtenemos el correo del usuario
		var persona Persona
		statusCodeGetPersonaByUsuarioID, errGetPersonaByUsuarioID := getPersonaByUsuarioID(c, pregunta.UsuarioID, &persona)
		if errGetPersonaByUsuarioID == nil && statusCodeGetPersonaByUsuarioID == 200 {
			// Enviamos el token de validacion al correo
			endpointSendRespuestaPreguntaMail, statusCodeSendRespuestaPreguntaMail, errSendRespuestaPreguntaMail := sendRespuestaPreguntaMail(c, persona.Correo, pregunta.Pregunta, pregunta.ID.Hex())
			if errSendRespuestaPreguntaMail != nil {
				go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Ocurrio un error al enviar el correo", endpointSendRespuestaPreguntaMail, nil, statusCodeSendRespuestaPreguntaMail, nil)
			} else {
				go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Envio de correo exitoso", endpointSendRespuestaPreguntaMail, nil, statusCodeSendRespuestaPreguntaMail, nil)
			}
		} else {
			go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Ocurrio al buscar la persona asociada a la pregunta", nil, nil, statusCodeGetPersonaByUsuarioID, errGetPersonaByUsuarioID)
		}
	} else {
		go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, nombreMetodo, "Ocurrio un error al buscar la pregunta", nil, nil, nil, err)
	}

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusCreated, "")
	return c.JSON(http.StatusCreated, &obj)
}

// UpdateRespuesta actualiza un registro
func (PreguntaHandler) UpdateRespuesta(c echo.Context) error {
	nombreMetodo := "UpdateRespuesta"
	defer c.Request().Body.Close()

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	preguntaID := c.Param(preguntaID)
	id := c.Param(ID)

	// Transformamos en requestBody en la entidad
	var obj Respuesta
	err := apiUtil.ConvertBodyToEntity(c.Request().Body, &obj)
	if err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.NoContent(http.StatusBadRequest)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj)

	// Validamos el request
	if err := apiUtil.ValidateStruct(obj); err != nil {
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusBadRequest, err.Error())
		return c.JSONBlob(http.StatusBadRequest, []byte(err.Error()))
	}

	// Actualizamos la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.UpdateRespuesta(preguntaID, id, &obj, username); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, &obj, auditoriaUtil.UPDATE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), obj, http.StatusOK, "")
	return c.JSON(http.StatusOK, &obj)
}

// DeleteRespuesta elimina un registro
func (PreguntaHandler) DeleteRespuesta(c echo.Context) error {
	nombreMetodo := "DeleteRespuesta"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	preguntaID := c.Param(preguntaID)
	id := c.Param(ID)

	// Eliminamos de la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.DeleteRespuesta(preguntaID, id); err != nil {
		if err == mgo.ErrNotFound {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// IncreaseRespuesta incrementa un campo
func (PreguntaHandler) IncreaseRespuesta(c echo.Context) error {
	nombreMetodo := "IncreaseRespuesta"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	username, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Logueamos
	go logger.PrintRequest(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil)

	// Obtenemos los parametros
	preguntaID := c.Param(preguntaID)
	id := c.Param(ID)
	field := c.QueryParam("field")
	value, _ := strconv.Atoi(c.QueryParam("value"))

	// Incrementamos el registro en la BD
	var preguntaRepository = PreguntaRepository{}
	if err := preguntaRepository.IncreaseRespuesta(preguntaID, id, field, value); err != nil {
		if err == mgo.ErrNotFound || err.Error() == dbConstantes.ErrorDatabaseInvalidID {
			// Logueamos
			go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusNoContent, err.Error())
			return c.JSON(http.StatusNoContent, dbConstantes.ErrorDatabaseRecordNotFound)
		}
		// Logueamos
		go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.ERROR, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusInternalServerError, err.Error())
		return c.NoContent(http.StatusInternalServerError)
	}

	// Auditamos
	go auditoriaUtil.Auditar(CollectionName, username, c.Request().Header, id, nil, auditoriaUtil.DELETE)

	// Logueamos
	go logger.PrintResponse(CollectionName, c.RealIP(), apiUtil.GetIPServer().String(), apiUtil.INFO, c.Request().Header, username, APIName, nombreMetodo, c.Path(), nil, http.StatusOK, "")
	return c.NoContent(http.StatusOK)
}

// ******** REPORTS ********

// GetReportPreguntasAndRespuestasByYear devuelve las preguntas y respuestas por mes
func (PreguntaHandler) GetReportPreguntasAndRespuestasByYear(c echo.Context) error {
	// nombreMetodo := "GetReportPreguntasAndRespuestasByYear"

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	year, _ := strconv.Atoi(c.Param("year"))

	// Mapeamos la respuesta
	months := make([]GetReportPreguntasAndRespuestas, 12)

	var preguntaRepository = PreguntaRepository{}
	for index := range months {
		// Buscamos las preguntas
		countPreguntas, errPreguntas := preguntaRepository.CountByMonthAndYear(index+1, year, "fechaCreacion")
		if errPreguntas != nil {
			countPreguntas = 0
		}

		// Buscamos las respuestas
		countRespuestas, errRespuestas := preguntaRepository.CountByMonthAndYear(index+1, year, "fechaUltimaRespuesta")
		if errRespuestas != nil {
			countRespuestas = 0
		}

		months[index] = GetReportPreguntasAndRespuestas{
			Preguntas:  countPreguntas,
			Respuestas: countRespuestas,
		}
	}
	return c.JSON(http.StatusOK, months)
}

// ******** OTROS ********

// getPersonaByUsuarioID obtiene una persona
func getPersonaByUsuarioID(c echo.Context, usuarioID string, persona *Persona) (int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPIUsuarios
	url := fmt.Sprintf("%s%s%s", endpoint, "/getPersonaByUsuarioId/", usuarioID)

	return apiUtil.GetResponse(apiUtil.Get, url, apiUtil.GetEndpointConfig().TimeoutAPIUsuarios, c.Request().Header, nil, &persona)
}

// asignPersonasToRespuestas asigna la persona que haya respondido a un array de respuestas
func asignPersonasToRespuestas(respuestas []Respuesta, c echo.Context) {
	if len(respuestas) > 0 {
		var wg sync.WaitGroup
		wg.Add(len(respuestas))

		for i := range respuestas {
			go func(i int) {
				defer wg.Done()
				var persona Persona

				statusCode, err := getPersonaByUsuarioID(c, respuestas[i].UsuarioID, &persona)
				if err != nil {
					go logger.PrintTrace(CollectionName, apiUtil.INFO, c.Request().Header, APIName, "asignPersonasToRespuestas", "Ocurrio un error al buscar la persona asociada a la respuesta", nil, nil, statusCode, err)
					// fmt.Println("Error buscando la persona asociada a la respuesta:", respuestas[i].UsuarioID, err)
				}
				if statusCode == 200 {
					respuestas[i].Persona = persona
				}
			}(i)
		}
		wg.Wait()
	}
}

// sendRespuestaPreguntaMail envia un email
func sendRespuestaPreguntaMail(c echo.Context, correoUsuario string, pregunta string, preguntaID string) (string, int, error) {
	endpoint := apiUtil.GetEndpointConfig().EndpointAPICorreos

	url := fmt.Sprintf("%s%s", endpoint, "/send")
	var correo correoService.Correo
	correo.Destinatario = correoUsuario
	correo.Asunto = config.GetConstantConfig().Email.EmailRespuestaPregunta.Asunto

	palabras := make(map[string]string)
	palabras["PREGUNTA"] = pregunta
	palabras["URL_PREGUNTA"] = config.GetConstantConfig().URLBase + "/preguntas/detail/" + preguntaID

	correo.Palabras = palabras
	correo.NombrePlantilla = config.GetConstantConfig().Email.EmailRespuestaPregunta.Plantilla

	statusCode, err := apiUtil.GetResponse(apiUtil.Post, url, apiUtil.GetEndpointConfig().TimeoutAPICorreos, c.Request().Header, &correo, nil)
	return url, statusCode, err
}
