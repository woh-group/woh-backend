package pregunta

// GetReportPreguntasAndRespuestas entidad
type GetReportPreguntasAndRespuestas struct {
	Preguntas  int `json:"preguntas"`
	Respuestas int `json:"respuestas"`
}
