package pregunta

import (
	"errors"
	"fmt"
	"time"

	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-preguntas/config"
	dbConstantes "gitlab.com/woh-group/woh-backend/db-mongo/constantes"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// CollectionName nombre de la coleccion
const CollectionName = "preguntas"

// PreguntaRepository objeto entidad
type PreguntaRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

// Parametros
var typeFields = map[string]string{
	"pregunta":             dbUtil.TypeString,
	"categoriaId":          dbUtil.TypeString,
	"usuarioId":            dbUtil.TypeString,
	"fechaUltimaRespuesta": dbUtil.TypeDate,
	"genero":               dbUtil.TypeString,
	"edad":                 dbUtil.TypeInteger,
	"visitas":              dbUtil.TypeInteger,
	"fechaCreacion": 		dbUtil.TypeDate,
}

func (repository *PreguntaRepository) initContext() {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContext(CollectionName, &conectionDB)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(CollectionName)
	}
}

// FindAll devuelve todos los registros
func (repository PreguntaRepository) FindAll(c echo.Context) (*[]Pregunta, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []Pregunta
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)

	return &objs, err
}

// FindByID devuelve un registro filtrado por Id
func (repository PreguntaRepository) FindByID(id string) (*Pregunta, error) {
	repository.initContext()
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return nil, errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	var obj Pregunta
	err := repository.c.FindId(bson.ObjectIdHex(id)).One(&obj)
	return &obj, err
}

// Count devuelve la cantidad de registros
func (repository PreguntaRepository) Count(c echo.Context) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}

// Create inserta un nuevo registro
func (repository PreguntaRepository) Create(obj *Pregunta, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	currentDate := time.Now()
	obj.ID = bson.NewObjectId()
	obj.Fecha = &currentDate
	obj.FechaCreacion = &currentDate
	obj.UsuarioCreacion = usuarioCreacion
	err := repository.c.Insert(obj)
	return err
}

// Update actualiza un registro
func (repository PreguntaRepository) Update(id string, obj *Pregunta, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	currentDate := time.Now()
	obj.Fecha = &currentDate
	obj.FechaModificacion = &currentDate
	obj.UsuarioModificacion = usuarioModificacion
	obj.ID = bson.ObjectIdHex(id)

	// err := repository.c.UpdateId(obj.ID, &obj)
	err := repository.c.UpdateId(obj.ID,
		bson.M{"$set": bson.M{
			"pregunta":             obj.Pregunta,
			"categoriaId":          obj.CategoriaID,
			"informacionAdicional": obj.InformacionAdicional,
			"genero":               obj.Genero,
			"edad":                 obj.Edad,
			"fecha":                obj.Fecha,
			"fechaModificacion":    obj.FechaModificacion,
			"usuarioModificacion":  obj.UsuarioModificacion,
		}})
	return err
}

// Delete elimina un registro
func (repository PreguntaRepository) Delete(id string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.RemoveId(bson.ObjectIdHex(id))
}

// Increase incrementa un campo
func (repository PreguntaRepository) Increase(id string, field string, cant int) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(id) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	return repository.c.UpdateId(bson.ObjectIdHex(id),
		bson.M{"$inc": bson.M{field: cant}})
}

// CreateMany agrega un array de objetos
func (repository PreguntaRepository) CreateMany(objs []Pregunta) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	// repository.c.RemoveAll(bson.M{})
	repository.c.DropCollection()

	for _, obj := range objs {
		err := repository.c.Insert(obj)
		if err != nil {
			return err
		}
	}
	return nil
}

// ******** RESPUESTAS ********

// CreateRespuesta inserta un nuevo registro
func (repository PreguntaRepository) CreateRespuesta(preguntaID string, obj *Respuesta, usuarioCreacion string) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	currentDate := time.Now()
	obj.ID = bson.NewObjectId()
	obj.FechaCreacion = &currentDate
	obj.UsuarioCreacion = usuarioCreacion

	change := bson.M{"$push": bson.M{"respuestas": obj}, "$set": bson.M{"fechaUltimaRespuesta": currentDate}}
	err := repository.c.UpdateId(bson.ObjectIdHex(preguntaID), change)
	return err
}

// UpdateRespuesta actualiza un registro
func (repository PreguntaRepository) UpdateRespuesta(preguntaID string, respuestaID string, obj *Respuesta, usuarioModificacion string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(preguntaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(respuestaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	currentDate := time.Now()
	obj.FechaModificacion = &currentDate
	obj.UsuarioModificacion = usuarioModificacion

	query := bson.M{"_id": bson.ObjectIdHex(preguntaID), "respuestas._id": bson.ObjectIdHex(respuestaID)}
	change := bson.M{"$set": bson.M{
		"respuestas.$.respuesta":         obj.Respuesta,
		"perfiles.$.fechaModificacion":   obj.FechaModificacion,
		"perfiles.$.usuarioModificacion": obj.UsuarioModificacion,
	}}
	err := repository.c.Update(query, change)
	return err
}

// DeleteRespuesta elimina un registro
func (repository PreguntaRepository) DeleteRespuesta(preguntaID string, respuestaID string) error {
	repository.initContext()
	defer repository.context.Close()

	if !bson.IsObjectIdHex(preguntaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(respuestaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	change := bson.M{"$pull": bson.M{"respuestas": bson.M{"_id": bson.ObjectIdHex(respuestaID)}}, "$set": bson.M{"fechaUltimaRespuesta": nil}}
	err := repository.c.UpdateId(bson.ObjectIdHex(preguntaID), change)
	return err
}

// IncreaseRespuesta incrementa un campo
func (repository PreguntaRepository) IncreaseRespuesta(preguntaID string, respuestaID string, field string, cant int) error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	if !bson.IsObjectIdHex(preguntaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	if !bson.IsObjectIdHex(respuestaID) {
		return errors.New(dbConstantes.ErrorDatabaseInvalidID)
	}

	query := bson.M{"_id": bson.ObjectIdHex(preguntaID), "respuestas._id": bson.ObjectIdHex(respuestaID)}
	change := bson.M{"$inc": bson.M{fmt.Sprintf("%s%s", "respuestas.$.", field): cant}}

	err := repository.c.Update(query, change)
	return err
}


// ******** REPORTS ********

// CountByMonthAndYear devuelve la cantidad de registros
func (repository PreguntaRepository) CountByMonthAndYear(month int, year int, field string) (int, error) {
	repository.initContext()
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Creamos los parametros
	startDate := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	endDate := time.Date(year, time.Month(month), 31, 11, 59, 59, 0, time.UTC)
	params := bson.M{field: bson.M{"$gte": startDate, "$lte": endDate}}

	count, err := repository.c.Find(params).Count()
	return count, err
}

// ******** OTROS ********

// RemoveAll elimina los registros de una coleccion
func (repository PreguntaRepository) RemoveAll() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Clear DB
	_, err := repository.c.RemoveAll(nil)
	return err
}

// DropCollection elimina la coleccion completa
func (repository PreguntaRepository) DropCollection() error {
	repository.initContext()
	if repository.err != nil {
		return repository.err
	}
	defer repository.context.Close()

	// Delete Coleccion
	err := repository.c.DropCollection()
	return err
}