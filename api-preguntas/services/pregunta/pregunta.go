package pregunta

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Pregunta entidad
type Pregunta struct {
	ID                   bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Pregunta             string        `bson:"pregunta" json:"pregunta" validate:"required"`
	CategoriaID          string        `bson:"categoriaId" json:"categoriaId"`
	InformacionAdicional string        `bson:"informacionAdicional" json:"informacionAdicional"`
	UsuarioID            string        `bson:"usuarioId" json:"usuarioId" validate:"required"`
	Genero               string        `bson:"genero" json:"genero" validate:"required"`
	Edad                 int           `bson:"edad" json:"edad" validate:"required"`
	Fecha                *time.Time    `bson:"fecha" json:"fecha"`
	FechaUltimaRespuesta *time.Time    `bson:"fechaUltimaRespuesta" json:"fechaUltimaRespuesta"`
	Visitas              int           `bson:"visitas" json:"visitas"`
	Respuestas           []Respuesta   `bson:"respuestas" json:"respuestas"`
	FechaCreacion        *time.Time    `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion      string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion    *time.Time    `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion  string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// Respuesta entidad
type Respuesta struct {
	ID                  bson.ObjectId `bson:"_id,omitempty" json:"_id"`
	Respuesta           string        `bson:"respuesta" json:"respuesta" validate:"required"`
	UsuarioID           string        `bson:"usuarioId" json:"usuarioId" validate:"required"`
	Likes               int           `bson:"likes" json:"likes"`
	Dislikes            int           `bson:"dislikes" json:"dislikes"`
	Persona             Persona       `bson:"persona" json:"persona"`
	FechaCreacion       *time.Time    `bson:"fechaCreacion" json:"fechaCreacion,omitempty"`
	UsuarioCreacion     string        `bson:"usuarioCreacion" json:"usuarioCreacion,omitempty"`
	FechaModificacion   *time.Time    `bson:"fechaModificacion" json:"fechaModificacion,omitempty"`
	UsuarioModificacion string        `bson:"usuarioModificacion" json:"usuarioModificacion,omitempty"`
}

// Persona entidad
type Persona struct {
	Nombres   string `bson:"nombres" json:"nombres"`
	Apellidos string `bson:"apellidos" json:"apellidos"`
	Correo    string `bson:"correo" json:"correo"`
}
