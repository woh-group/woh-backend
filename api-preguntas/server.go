// API Preguntas
//
// Documentación de la API
//
//     Schemes: http, https
//     Host: localhost:3003
//     BasePath: /api/preguntas
//     Version: 1.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: https://osmandi.now.sh
//
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package main

import (
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	// "github.com/swaggo/echo-swagger"
	// _ "gitlab.com/woh-group/woh-backend/api-preguntas/swagger"

	"gitlab.com/woh-group/woh-backend/api-preguntas/config"
	"gitlab.com/woh-group/woh-backend/api-preguntas/routers"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
	auditoriaConfig "gitlab.com/woh-group/woh-backend/util-auditoria/config"
	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
	loggerConfig "gitlab.com/woh-group/woh-backend/util-logger/config"
	logger "gitlab.com/woh-group/woh-backend/util-logger/util"
)

func init() {
	// Cargamos la configuracion inicial
	config.LoadConfigFile()

	// Mapeamos las conexiones
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	loggerConfig.LoadConfigDB(&conectionDB)
	auditoriaConfig.LoadConfigDB(&conectionDB)
}

func main() {
	fmt.Println("")
	fmt.Println("*********************************************")
	fmt.Println("*************************** API PREGUNTAS ***")
	fmt.Println("*********************************************")
	fmt.Println("_____________________________________________")
	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:  []string{"*"},
		AllowMethods:  []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE, echo.OPTIONS},
		ExposeHeaders: []string{apiUtil.RequestID},
	}))
	e.Use(middlewareValidarPermiso)

	routers.InitRoutes(e)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, Api - Preguntas!")
	})

	e.Logger.Fatal(e.Start(":3003"))
}

func middlewareValidarPermiso(next echo.HandlerFunc) echo.HandlerFunc {
	// return a HandlerFunc
	return func(c echo.Context) error {
		// Mapeamos los headers al response
		c.Response().Header().Set(apiUtil.RequestID, c.Request().Header.Get(apiUtil.RequestID))

		// Obtenemos el nombre de la api
		nombreAPI := apiUtil.GetAPIToPath("preguntas", c.Path())

		// Obtenemos el token y username
		token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
		username, errUsername := authUtil.GetUsernameByToken(token)

		// Validamos el username
		if errUsername != nil {
			go logger.PrintLog(nombreAPI, logger.ERROR, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Method, c.Request().Header, username, c.Request().Host+c.Path(), http.StatusUnauthorized, authUtil.ErrorInvalidToken)
			return &echo.HTTPError{
				Code:    http.StatusUnauthorized,
				Message: authUtil.ErrorInvalidToken,
			}
		}

		// Validamos los headers obligatorios
		if !apiUtil.ValidarHeaders(c.Request().Header) {
			go logger.PrintLog(nombreAPI, logger.ERROR, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Method, c.Request().Header, username, c.Request().Host+c.Path(), http.StatusBadRequest, apiUtil.ErrorHeaderNotFound)
			return &echo.HTTPError{
				Code:    http.StatusBadRequest,
				Message: apiUtil.ErrorHeaderNotFound,
			}
		}

		// Validamos el acceso
		// acceso, err := apis.ValidarPermisos(c.Request().Method, c.Path(), token)
		// fmt.Println("acceso: ", acceso, err)
		// if err != nil || !acceso {
		// 	go logger.PrintLog(nombreAPI, logger.ERROR, c.RealIP(), apiUtil.GetIPServer().String(), c.Request().Method, c.Request().Header, username, c.Request().Host+c.Path(), http.StatusForbidden, authUtil.ErrorAccesoDenegado)
		// 	return &echo.HTTPError{
		// 		Code:    http.StatusForbidden,
		// 		Message: authUtil.ErrorAccesoDenegado,
		// 	}
		// }
		return next(c)
	}
}
