# Descripción

Proyecto api de preguntas

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-preguntas/...

## Crear Modulo

export GO111MODULE=auto
export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-preguntas

## Docker

docker build -t api-preguntas .

docker build --no-cache -t api-preguntas .

docker run -it api-preguntas bash

### Docker Linux

docker run -it --name api-preguntas -p 3003:3003 api-preguntas

### Docker Windows

winpty docker run -it --name api-preguntas -p 3003:3003 api-preguntas

### Docker con enviroment

#### En Linux

docker run -it --name api-preguntas -p 3003:3003 -v ${WOH_CONFIG_PATH}:/config -e WOH_CONFIG_PATH=/config api-preguntas
docker run -it --name api-preguntas -p 3003:3003 -v /home/ubuntu/woh/_CONFIG_FILES:/config -e WOH_CONFIG_PATH=/config api-preguntas

#### En Windows

docker run -it --name api-preguntas -p 3003:3003 -v "%WOH_CONFIG_PATH%":/config -e WOH_CONFIG_PATH=/config api-preguntas