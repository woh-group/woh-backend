package routers

import (
	echo "github.com/labstack/echo/v4"

	categoriaService "gitlab.com/woh-group/woh-backend/api-preguntas/services/categoria"
	preguntaService "gitlab.com/woh-group/woh-backend/api-preguntas/services/pregunta"
)

// PATH de la api
const PATH = "/api/preguntas"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {

	// create groups
	preguntaGroup := e.Group(PATH + "/preguntas")
	categoriaGroup := e.Group(PATH + "/categorias")

	preguntaService.SetRouters(preguntaGroup)
	categoriaService.SetRouters(categoriaGroup)
}
