package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/woh-group/woh-backend/api-preguntas/config"
	categoriaService "gitlab.com/woh-group/woh-backend/api-preguntas/services/categoria"
)

func main() {
	// Cargamos la configuracion inicial
	config.LoadConfigFile()

	var categorias, errCategoria = getCategorias()
	if errCategoria != nil {
		fmt.Println("ERROR ->", errCategoria)
	}
	// fmt.Println("CATEGORIAS ->", categorias)
	var paisRepository = categoriaService.CategoriaRepository{}
	if errCategoria = paisRepository.CreateMany(categorias); errCategoria != nil {
		fmt.Println("ERROR ->", errCategoria)
	} else {
		fmt.Println("CATEGORIAS cargados exitosamente!")
	}
}

func getCategorias() ([]categoriaService.Categoria, error) {
	objsJSON, err := ioutil.ReadFile("jsons/categorias.json")

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	var objs []categoriaService.Categoria
	err = json.Unmarshal(objsJSON, &objs)

	if err != nil {
		// fmt.Println("ERROR ->", err)
		return nil, err
	}

	for i := range objs {
		objs[i].ID = bson.NewObjectId()
		objs[i].FechaCreacion = time.Now()
		objs[i].UsuarioCreacion = "admin"
		objs[i].FechaModificacion = time.Time{}
	}

	return objs, nil
}
