package routers

import (
	echo "github.com/labstack/echo/v4"

	traceService "gitlab.com/woh-group/woh-backend/api-trace/services/trace"
)

// PATH de la api
const PATH = "/api/trace"

// InitRoutes inicializa las rutas
func InitRoutes(e *echo.Echo) {
	// create groups
	traceGroup := e.Group(PATH)
	traceService.SetRouters(traceGroup)
}
