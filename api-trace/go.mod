module gitlab.com/woh-group/woh-backend/api-trace

require (
	github.com/aws/aws-sdk-go v1.19.34
	github.com/gin-gonic/gin v1.4.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.5
	github.com/swaggo/swag v1.5.0
	gitlab.com/woh-group/woh-backend/api-seguridad v0.0.0-20190507063013-21ccf4eb5ef7 // indirect
	gitlab.com/woh-group/woh-backend/db-mongo v0.0.0-20190604010528-17c6582ed597
	gitlab.com/woh-group/woh-backend/util v0.0.0-20190521032513-20f3e53643c2
	gitlab.com/woh-group/woh-backend/util-api v0.0.0-20190521032513-20f3e53643c2
	gitlab.com/woh-group/woh-backend/util-auditoria v0.0.0-20190521032513-20f3e53643c2
	gitlab.com/woh-group/woh-backend/util-auth v0.0.0-20190521032513-20f3e53643c2
	gitlab.com/woh-group/woh-backend/util-logger v0.0.0-20190521032513-20f3e53643c2
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190520210107-018c4d40a106 // indirect
	golang.org/x/sys v0.0.0-20190520201301-c432e742b0af // indirect
	golang.org/x/tools v0.0.0-20190520220859-26647e34d3c0 // indirect
	google.golang.org/appengine v1.6.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/validator.v2 v2.0.0-20180514200540-135c24b11c19
	gopkg.in/yaml.v2 v2.2.2
)
