package trace

import (
	echo "github.com/labstack/echo/v4"
	"gopkg.in/mgo.v2"

	config "gitlab.com/woh-group/woh-backend/api-trace/config"
	db "gitlab.com/woh-group/woh-backend/db-mongo/server"
	dbUtil "gitlab.com/woh-group/woh-backend/db-mongo/util"
	apiUtil "gitlab.com/woh-group/woh-backend/util-api/util"
)

// TraceRepository objeto entidad
type TraceRepository struct {
	c       *mgo.Collection
	context *db.Context
	err     error
}

// Parametros
var typeFields = map[string]string{
	"api":        dbUtil.TypeString,
	"appId":      dbUtil.TypeString,
	"fecha":      dbUtil.TypeDate,
	"ipCliente":  dbUtil.TypeString,
	"ipServidor": dbUtil.TypeString,
	"level":      dbUtil.TypeString,
	"metodo":     dbUtil.TypeString,
	"requestId":  dbUtil.TypeString,
	"statusCode": dbUtil.TypeInteger,
	"tipo":       dbUtil.TypeString,
	"username":   dbUtil.TypeString,
}

func (repository *TraceRepository) initContext(collectionName string) {
	conectionDB := db.ConectionDB(config.GetConnectionConfig().Database)
	repository.context, repository.err = db.NewContext(collectionName, &conectionDB)
	if repository.err == nil {
		repository.c = repository.context.DBCollection(collectionName)
	}
}

// FindAll devuelve todos los registros
func (repository TraceRepository) FindAll(c echo.Context, collectionName string) (*[]interface{}, error) {
	repository.initContext(collectionName)
	if repository.err != nil {
		return nil, repository.err
	}
	defer repository.context.Close()

	var objs []interface{}
	var err error

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)
	sort := apiUtil.GetParametroSort(c)
	pageNumber, pageSize := apiUtil.GetParametrosPaginacion(c)

	query := dbUtil.CreateQuery(repository.c, params, sort, pageNumber, pageSize)
	err = query.All(&objs)
	return &objs, err
}

// Count devuelve la cantidad de registros
func (repository TraceRepository) Count(c echo.Context, collectionName string) (int, error) {
	repository.initContext(collectionName)
	if repository.err != nil {
		return 0, repository.err
	}
	defer repository.context.Close()

	// Obtenemos el parametro
	params := dbUtil.GetParametrosFiltro(c.QueryParams(), typeFields)

	count, err := repository.c.Find(params).Count()
	return count, err
}
