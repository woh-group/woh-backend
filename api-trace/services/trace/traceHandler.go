package trace

import (
	"net/http"

	echo "github.com/labstack/echo/v4"

	authUtil "gitlab.com/woh-group/woh-backend/util-auth/util"
)

// UsuarioHandler objeto
type TraceHandler struct{}

// Variables globales
const (
	TRACE = "trace"
	API      = "api"
	APIName = "trace"
	PathAuditoria = "auditoria"
	PathLogs = "logs"
)


func (TraceHandler) GetAll(c echo.Context) error {
	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)
	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	trace := c.Param(TRACE)
	api := c.Param(API)
	var collectionName string

	if trace == PathAuditoria {
		collectionName = api + "Aud"
	} else if trace == PathLogs {
		collectionName = api + "Logs"
	}

	// Buscamos en la BD
	var traceRepository = TraceRepository{}
	objs, err := traceRepository.FindAll(c, collectionName)
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, objs)
}

// Count obtiene la cantidad de registros
func (TraceHandler) Count(c echo.Context) error {

	// Obtenemos el token y username
	token := authUtil.GetTokenByHeader(c.Request().Header.Get(echo.HeaderAuthorization))
	_, errUsername := authUtil.GetUsernameByToken(token)

	// Validamos el username
	if errUsername != nil {
		return c.JSON(http.StatusUnauthorized, authUtil.ErrorInvalidToken)
	}

	// Obtenemos los parametros
	trace := c.Param(TRACE)
	api := c.Param(API)
	var collectionName string

	if trace == PathAuditoria {
		collectionName = api + "Aud"
	} else if trace == PathLogs {
		collectionName = api + "Logs"
	}

	// Buscamos en la BD
	var traceRepository = TraceRepository{}
	count, err := traceRepository.Count(c, collectionName)
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}
		
	return c.JSON(http.StatusOK, count)
}
