package trace

import (
	echo "github.com/labstack/echo/v4"
)

// SetRouters setea los routers
func SetRouters(g *echo.Group) {
	var handler = TraceHandler{}

	g.GET("/:trace/:api", handler.GetAll)
	g.GET("/:trace/:api/count", handler.Count)
}
