# Descripción

Proyecto api trace

## Obtener todos los paquetes

go get -d -u gitlab.com/woh-group/woh-backend/api-trace/...

## Crear Modulo

export GO111MODULE=auto
export GO111MODULE=on

go mod init gitlab.com/woh-group/woh-backend/api-trace

## Swagger generar swagger.json

https://goswagger.io/

Escanear los modelos

swagger generate spec -o ./swagger.json --scan-models

Probar online

swagger serve -F=swagger swagger.json

## Docker

docker build -t api-trace .

docker build --no-cache -t api-trace .

docker run -it api-trace bash

### Docker Linux

docker run -it --name api-trace -p 3004:3004 api-trace

### Docker Windows

winpty docker run -it --name api-trace -p 3004:3004 api-trace

### Docker con enviroment

#### En Linux

docker run -it --name api-trace -p 3004:3004 -v ${WOH_CONFIG_PATH}:/config -e WOH_CONFIG_PATH=/config api-trace
docker run -it --name api-trace -p 3004:3004 -v /home/ubuntu/woh/_CONFIG_FILES:/config -e WOH_CONFIG_PATH=/config api-trace

#### En Windows

docker run -it --name api-trace -p 3004:3004 -v "%WOH_CONFIG_PATH%":/config -e WOH_CONFIG_PATH=/config api-trace